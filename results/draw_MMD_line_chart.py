# importing package
import matplotlib.pyplot as plt
import os


def read_file(file_name):
    dsgc = []
    deep_nc = []

    file = open(file_name, 'r')
    Lines = file.readlines()

    count = 0
    # Strips the newline character
    for line in Lines:
        if count > 0:
            line_split_list = line.split('\t')
            line_split_list[1] = line_split_list[1][:-1]
            line_split_list = [float(i) for i in line_split_list]
            dsgc.append(line_split_list[0])
            deep_nc.append(line_split_list[1])
        count += 1
    x = [i + 1 for i in range(len(dsgc))]
    # create data
    print(x)
    print(dsgc)
    print(deep_nc)
    return x, dsgc, deep_nc


data_set_name = 'PTC'

file_name = data_set_name + '_deg_mmd.txt'
x, deg_mmd_dsgc, deg_mmd_deep_nc = read_file(file_name)

file_name = data_set_name + '_clus_mmd.txt'
x, clus_mmd_dsgc, clus_mmd_deep_nc = read_file(file_name)

file_name = data_set_name + '_spec_mmd.txt'
x, spec_mmd_dsgc, spec_mmd_deep_nc = read_file(file_name)

fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
fig.suptitle('MMD Scores, ' + data_set_name + ' Dataset')

ax1.plot(x, deg_mmd_dsgc, '-o', label="DSGC_RNN$^+$ (ours)", color='b')
ax1.plot(x, deg_mmd_deep_nc, '-o', label="DeepNC_L", color='orange')
ax1.set(xlabel="Number of Missing Nodes", ylabel="Deg. MMD")

ax2.plot(x, clus_mmd_dsgc, '-o', label="DSGC_RNN$^+$ (ours)", color='b')
ax2.plot(x, clus_mmd_deep_nc, '-o', label="DeepNC_L", color='orange')
ax2.set(xlabel="Number of Missing Nodes", ylabel="Clus. MMD")

ax3.plot(x, spec_mmd_dsgc, '-o', label="DSGC_RNN$^+$ (ours)", color='b')
ax3.plot(x, spec_mmd_deep_nc, '-o', label="DeepNC_L", color='orange')
ax3.set(xlabel="Number of Missing Nodes", ylabel="Spec. MMD")

fig.set_figheight(3.5)
fig.set_figwidth(13)
fig.tight_layout(pad=3.8)
plt.legend()
plt.legend(loc=(0.35, 1.03))
plt.savefig(data_set_name + "_MMD_result_line_chart" + ".pdf")
