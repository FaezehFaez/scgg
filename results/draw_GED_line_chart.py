# importing package
import matplotlib.pyplot as plt
import os

dsgc = []
dsgc_error = []
deep_nc = []
deep_nc_error = []
# Using readlines()
data_set_name = 'CiteSeer'
file = open(data_set_name + '_Results.txt', 'r')
Lines = file.readlines()

count = 0
# Strips the newline character
for line in Lines:
    if count > 0:
        line_split_list = line.split('\t')
        line_split_list[3] = line_split_list[3][:-1]
        line_split_list = [float(i) for i in line_split_list]
        dsgc.append(line_split_list[0])
        dsgc_error.append(line_split_list[1])
        deep_nc.append(line_split_list[2])
        deep_nc_error.append(line_split_list[3])
    count += 1
x = [i + 1 for i in range(len(dsgc))]
# create data
print(x)
print(dsgc)
print(dsgc_error)
print(deep_nc)
print(deep_nc_error)
# x = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# dsgc = [0.056, 0.129, 0.122, 0.129, 0.162, 0.169, 0.178, 0.198, 0.254]
# dsgc_error = [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]
# deep_nc = [0.07, 0.13, 0.169, 0.183, 0.205, 0.227, 0.255, 0.264, 0.273]
# deep_nc_error = [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]

# plot lines
plt.plot(x, dsgc, '--', label="DSGC_RNN$^+$ (ours)", color='b')
plt.errorbar(x, dsgc, dsgc_error, linestyle='None', marker='s', color='b', capsize=4)
plt.plot(x, deep_nc, '--', label="DeepNC_L", color='orange')
plt.errorbar(x, deep_nc, deep_nc_error, linestyle='None', marker='d', color='orange', capsize=4)

plt.legend()
plt.xlabel('Number of Missing Nodes')
plt.ylabel('GED')
plt.suptitle('GED Score, ' + data_set_name + ' Dataset')
plt.savefig(data_set_name + "_GED_result_line_chart" + ".pdf")
