# importing package
import matplotlib.pyplot as plt
import os


def draw_line_with_sd_bands(ax, x, y, y_sd, label, color, plot_type, alpha=0.2):
    ax.plot(x, y, '-', label=label, color=color, linewidth=.9, marker='o', markersize='6')
    lower = [x - y for x, y in zip(y, y_sd)]
    upper = [x + y for x, y in zip(y, y_sd)]
    # ax.plot(x, lower, '--', linewidth=.2, color=color, alpha=0.7)
    # ax.plot(x, upper, '--', linewidth=.2, color=color, alpha=0.7)
    ax.fill_between(x, lower, upper, color=color, alpha=alpha)
    # if plot_type == 'one':
    #     ax.errorbar(x, y, y_sd, linestyle='None', color=color, capsize=1.2, elinewidth=0.01)
    return


def read_data(data_set_name):
    if more_nodes:
        file = open(data_set_name + '_Results_V2_more_nodes.txt', 'r')
    else:
        file = open(data_set_name + '_Results_V2.txt', 'r')
    Lines = file.readlines()

    dsgc = []
    dsgc_error = []
    deep_nc = []
    deep_nc_error = []
    graphrnn_s = []
    graphrnn_s_error = []
    graphrnn = []
    graphrnn_error = []
    kronem = []
    kronem_error = []
    evograph_v0 = []
    evograph_v0_error = []
    evograph_v1 = []
    evograph_v1_error = []
    evograph_v2 = []
    evograph_v2_error = []

    count = 0
    for line in Lines:
        if count > 0:
            line_split_list = line.split('\t')
            line_split_list[len(line_split_list) - 1] = line_split_list[len(line_split_list) - 1][:-1]
            line_split_list = [float(i) for i in line_split_list]
            dsgc.append(line_split_list[0])
            dsgc_error.append(line_split_list[1])
            deep_nc.append(line_split_list[2])
            deep_nc_error.append(line_split_list[3])
            graphrnn_s.append(line_split_list[4])
            graphrnn_s_error.append(line_split_list[5])
            graphrnn.append(line_split_list[6])
            graphrnn_error.append(line_split_list[7])
            kronem.append(line_split_list[8])
            kronem_error.append(line_split_list[9])
            evograph_v0.append(line_split_list[10])
            evograph_v0_error.append(line_split_list[11])
            evograph_v1.append(line_split_list[12])
            evograph_v1_error.append(line_split_list[13])
            evograph_v2.append(line_split_list[14])
            evograph_v2_error.append(line_split_list[15])
        count += 1
    return dsgc, dsgc_error, deep_nc, deep_nc_error, graphrnn_s, graphrnn_s_error, \
           graphrnn, graphrnn_error, kronem, kronem_error, evograph_v0, evograph_v0_error, \
           evograph_v1, evograph_v1_error, evograph_v2, evograph_v2_error


def draw_evograph(axs):
    draw_line_with_sd_bands(axs, x, evograph_v0, evograph_v0_error, "EvoGraph (V0)", '#00aedb', plot_type, alpha=0.1)
    draw_line_with_sd_bands(axs, x, evograph_v1, evograph_v1_error, "EvoGraph (V1)", '#a0b84d', plot_type, alpha=0.1)
    draw_line_with_sd_bands(axs, x, evograph_v2, evograph_v2_error, "EvoGraph (V2)", '#d5467b', plot_type, alpha=0.1)
    fontsize = 10
    plt.legend(loc='lower center', bbox_to_anchor=(0.5, -0.4), ncol=3, borderaxespad=0, fancybox=True, shadow=True,
               edgecolor="black", prop={'size': fontsize})
    plt.subplots_adjust(bottom=0.27)

    axs.xlabel('Number of New Nodes', fontsize=fontsize)
    axs.ylabel('GED', fontsize=fontsize + 1)

    axs.suptitle('GED Score, ' + data_set_name + ' Dataset')

    axs.tick_params(axis='both', which='major', labelsize=fontsize)
    if more_nodes:
        plt.savefig(data_set_name + '_more_nodes' + "_GED_result_line_chart_evograph" + ".pdf")
    else:
        plt.savefig(data_set_name + "_GED_result_line_chart_evograph" + ".pdf")
    return


def draw_plots(axs, label, plot_type):
    if plot_type == 'one':
        draw_line_with_sd_bands(axs, x, dsgc, dsgc_error, label, '#ff0000', plot_type)
        draw_line_with_sd_bands(axs, x, deep_nc, deep_nc_error, "DeepNC", '#c04df9', plot_type, alpha=0.1)
        draw_line_with_sd_bands(axs, x, graphrnn_s, graphrnn_s_error, "GraphRNN-S", '#52d053', plot_type, alpha=0.18)
        draw_line_with_sd_bands(axs, x, graphrnn, graphrnn_error, "GraphRNN", '#ffa700', plot_type, alpha=0.14)
        draw_line_with_sd_bands(axs, x, kronem, kronem_error, "KronEM", '#398564', plot_type, alpha=0.1)
        draw_line_with_sd_bands(axs, x, evograph_v0, evograph_v0_error, "EvoGraph", '#00aedb', plot_type,
                                alpha=0.1)

        fontsize = 10
        plt.legend(loc='lower center', bbox_to_anchor=(0.5, -0.4), ncol=3, borderaxespad=0, fancybox=True, shadow=True,
                   edgecolor="black", prop={'size': fontsize})
        plt.subplots_adjust(bottom=0.27)

        axs.xlabel('Number of New Nodes', fontsize=fontsize)
        axs.ylabel('GED', fontsize=fontsize + 1)

        axs.suptitle('GED Score, ' + data_set_name + ' Dataset')

        axs.tick_params(axis='both', which='major', labelsize=fontsize)
        # axs.legend(loc='lower center', bbox_to_anchor=(0.5, -0.4), ncol=2, fancybox=True, shadow=True,
        #            edgecolor="black", prop={'size': 6})
    else:
        draw_line_with_sd_bands(axs[0, 0], x, dsgc, dsgc_error, label, '#ff0000', plot_type)
        draw_line_with_sd_bands(axs[0, 1], x, dsgc, dsgc_error, label, '#ff0000', plot_type)
        draw_line_with_sd_bands(axs[1, 0], x, dsgc, dsgc_error, label, '#ff0000', plot_type)
        draw_line_with_sd_bands(axs[1, 1], x, dsgc, dsgc_error, label, '#ff0000', plot_type)
        draw_line_with_sd_bands(axs[0, 1], x, deep_nc, deep_nc_error, "DeepNC", '#c04df9', plot_type, alpha=0.1)
        draw_line_with_sd_bands(axs[1, 1], x, graphrnn_s, graphrnn_s_error, "GraphRNN-S", '#52d053', plot_type,
                                alpha=0.18)
        draw_line_with_sd_bands(axs[1, 1], x, graphrnn, graphrnn_error, "GraphRNN", '#ffa700', plot_type, alpha=0.14)
        draw_line_with_sd_bands(axs[0, 0], x, kronem, kronem_error, "KronEM", '#398564', plot_type, alpha=0.1)
        draw_line_with_sd_bands(axs[1, 0], x, evograph_v0, evograph_v0_error, "EvoGraph", '#00aedb', plot_type,
                                alpha=0.1)
        lines_labels = [ax.get_legend_handles_labels() for ax in fig.axes]
        lines, labels = [sum(lol, []) for lol in zip(*lines_labels)]
        # fig.legend(lines, labels, loc='lower center', ncol=3, borderaxespad=0, prop={'size': 8})
        # plt.legend(loc='lower center', bbox_to_anchor=(0.5, -0.4), ncol=3, borderaxespad=0)
        # , prop={'size': 6}
        # plt.subplots_adjust(bottom=0.3)
        # plt.xlabel('Number of New Nodes', fontsize=8)
        # plt.ylabel('GED', fontsize=8)
        fontsize = 8
        for ax in axs.flat:
            ax.set_xlabel('Number of New Nodes', fontsize=fontsize)
            ax.set_ylabel('GED', fontsize=fontsize + 1)
        fig.suptitle('GED Score, ' + data_set_name + ' Dataset')
        plt.subplots_adjust(left=0.1,
                            bottom=0.1,
                            right=0.9,
                            top=0.9,
                            wspace=0.3,
                            hspace=0.21)
        axs[0, 0].get_xaxis().set_visible(False)
        axs[0, 1].get_xaxis().set_visible(False)

        axs[0, 0].tick_params(axis='both', which='major', labelsize=fontsize)
        axs[1, 0].tick_params(axis='both', which='major', labelsize=fontsize)
        axs[0, 1].tick_params(axis='both', which='major', labelsize=fontsize)
        axs[1, 1].tick_params(axis='both', which='major', labelsize=fontsize)
        axs[0, 0].legend(loc='upper center', bbox_to_anchor=(0.5, 1.11),
                         ncol=2, fancybox=True, shadow=True, edgecolor="black", prop={'size': 6})
        axs[1, 0].legend(loc='upper center', bbox_to_anchor=(0.5, 1.11),
                         ncol=2, fancybox=True, shadow=True, edgecolor="black", prop={'size': 6})
        axs[0, 1].legend(loc='upper center', bbox_to_anchor=(0.5, 1.11),
                         ncol=2, fancybox=True, shadow=True, edgecolor="black", prop={'size': 6})
        axs[1, 1].legend(loc='upper center', bbox_to_anchor=(0.5, 1.17),
                         ncol=2, fancybox=True, shadow=True, edgecolor="black", prop={'size': 6})
    if more_nodes:
        plt.savefig(data_set_name + '_more_nodes' + "_GED_result_line_chart_V2_" + plot_type + ".pdf")
    else:
        plt.savefig(data_set_name + "_GED_result_line_chart_V2_" + plot_type + ".pdf")
    return


if __name__ == '__main__':
    data_set_name = 'Enzymes'
    more_nodes = False
    dsgc, dsgc_error, deep_nc, deep_nc_error, graphrnn_s, graphrnn_s_error, \
    graphrnn, graphrnn_error, kronem, kronem_error, evograph_v0, evograph_v0_error, \
    evograph_v1, evograph_v1_error, evograph_v2, evograph_v2_error = read_data(data_set_name)

    if more_nodes:
        x = [(i + 1) * 10 for i in range(len(dsgc))]
    else:
        x = [i + 1 for i in range(len(dsgc))]

    fig, axs = plt.subplots(2, 2)
    # plot lines
    our_model_name = 'SCGG (ours)'
    plot_type = 'multi'
    draw_plots(axs, our_model_name, plot_type)
    plot_type = 'one'
    fig, axs = plt.subplots(1, 1)
    draw_plots(plt, our_model_name, plot_type)
    fig, axs = plt.subplots(1, 1)
    draw_evograph(plt)
    # draw_line_with_sd_bands(axs[1, 0], x, evograph_v1, evograph_v1_error, "EvoGraph(V1)", 'y')
    # draw_line_with_sd_bands(axs[1, 0], x, evograph_v2, evograph_v2_error, "EvoGraph(V2)", 'k')
