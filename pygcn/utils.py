import numpy as np


def prepare_data(adj, features):
    batch_size = adj.shape[0]
    for i in range(batch_size):
        # build symmetric adjacency matrix
        adj[i] = (adj[i] == adj[i].T) * adj[i] + (adj[i] != adj[i].T) * np.ones(adj[i].shape)

        features[i] = normalize(features[i])
        adj[i] = normalize(adj[i] + np.identity(adj[i].shape[0]))
    return adj, features


def normalize(mx):
    """Row-normalize sparse matrix"""
    rowsum = np.array(mx.sum(1))
    r_inv = np.power(rowsum, -1).flatten()
    r_inv[np.isinf(r_inv)] = 0.
    r_mat_inv = np.zeros((r_inv.shape[0], r_inv.shape[0]), float)
    np.fill_diagonal(r_mat_inv, r_inv)
    mx = r_mat_inv.dot(mx)
    return mx
