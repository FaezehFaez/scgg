import glob
import os
import pickle
import shutil
import statistics

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import scipy.io
import torch
from numpy import count_nonzero
from sklearn import metrics


def graph_show(G, title, colors):
    pos = nx.spring_layout(G, scale=2)
    nx.draw(G, pos, node_color=colors)
    fig = plt.gcf()
    fig.canvas.set_window_title(title)
    plt.show()
    plt.savefig('foo.png')


def graph_statistics(graphs):
    sparsity = 0
    matrix_size_list = []
    number_of_fully_connected_graphs = 0
    for i in range(len(graphs)):
        numpy_matrix = nx.to_numpy_matrix(graphs[i])
        non_zero = np.count_nonzero(numpy_matrix)
        if non_zero == numpy_matrix.shape[0] * numpy_matrix.shape[1] - numpy_matrix.shape[1]:
            number_of_fully_connected_graphs += 1
        sparsity += 1.0 - (count_nonzero(numpy_matrix) / float(numpy_matrix.size))
        matrix_size_list.append(numpy_matrix.shape[0])
    smallest_graph_size = min(matrix_size_list)
    largest_graph_size = max(matrix_size_list)
    graph_size_std = statistics.stdev(matrix_size_list)
    sparsity /= len(graphs)
    print("*** smallest_graph_size = " + str(smallest_graph_size) +
          "   *** largest_graph_size = " + str(largest_graph_size) +
          "   *** mean_graph_size = " + str(statistics.mean(matrix_size_list)) +
          "   *** graph_size_std = " + str(graph_size_std) +
          "   *** average_graph_sparsity = " + str(sparsity))
    print("*** total_number_of_graphs = " + str(len(graphs)) +
          "   *** number_of_fully_connected_graphs = " + str(number_of_fully_connected_graphs))
    return


# exactly the same implementation as https://cs.stanford.edu/people/jure/pubs/gin-iclr19.pdf
class S2VGraph(object):
    def __init__(self, g, label, node_tags=None, node_features=None):
        '''
            g: a networkx graph
            label: an integer graph label
            node_tags: a list of integer node tags
            node_features: a torch float tensor, one-hot representation of the tag that is used as input to neural nets
            edge_mat: a torch long tensor, contain edge list, will be used to create torch sparse tensor
            neighbors: list of neighbors (without self-loop)
        '''
        self.label = label
        self.g = g
        self.node_tags = node_tags
        self.neighbors = []
        self.node_features = 0
        self.edge_mat = 0

        self.max_neighbor = 0


# the same implementation as https://cs.stanford.edu/people/jure/pubs/gin-iclr19.pdf except for the return values (
# the last 6 lines of the code) and the graph size pruning (if prune_graph_size != -1 and g.number_of_nodes() >
# prune_graph_size:)
def load_data(dataset, degree_as_tag, prune_graph_size, min_graph_size):
    '''
        dataset: name of dataset
        test_proportion: ratio of test train split
        seed: random seed for random splitting of dataset
    '''

    print('loading data')
    g_list = []
    label_dict = {}
    feat_dict = {}

    with open('../dataset/%s/%s.txt' % (dataset, dataset), 'r') as f:
        n_g = int(f.readline().strip())
        for i in range(n_g):
            row = f.readline().strip().split()
            n, l = [int(w) for w in row]
            if not l in label_dict:
                mapped = len(label_dict)
                label_dict[l] = mapped
            g = nx.Graph()
            node_tags = []
            node_features = []
            n_edges = 0
            for j in range(n):
                g.add_node(j)
                row = f.readline().strip().split()
                tmp = int(row[1]) + 2
                if tmp == len(row):
                    # no node attributes
                    row = [int(w) for w in row]
                    attr = None
                else:
                    row, attr = [int(w) for w in row[:tmp]], np.array([float(w) for w in row[tmp:]])
                if not row[0] in feat_dict:
                    mapped = len(feat_dict)
                    feat_dict[row[0]] = mapped
                node_tags.append(feat_dict[row[0]])

                if tmp > len(row):
                    node_features.append(attr)

                n_edges += row[1]
                for k in range(2, len(row)):
                    g.add_edge(j, row[k])

            if (prune_graph_size != -1 and g.number_of_nodes() > prune_graph_size) or (
                    g.number_of_nodes() < min_graph_size):
                continue
            if node_features != []:
                node_features = np.stack(node_features)
                node_feature_flag = True
            else:
                node_features = None
                node_feature_flag = False

            assert len(g) == n

            g_list.append(S2VGraph(g, l, node_tags))

    # add labels and edge_mat
    for g in g_list:
        g.neighbors = [[] for i in range(len(g.g))]
        for i, j in g.g.edges():
            g.neighbors[i].append(j)
            g.neighbors[j].append(i)
        degree_list = []
        for i in range(len(g.g)):
            g.neighbors[i] = g.neighbors[i]
            degree_list.append(len(g.neighbors[i]))
        g.max_neighbor = max(degree_list)

        g.label = label_dict[g.label]

        edges = [list(pair) for pair in g.g.edges()]
        edges.extend([[i, j] for j, i in edges])

        deg_list = list(dict(g.g.degree(range(len(g.g)))).values())
        g.edge_mat = torch.LongTensor(edges).transpose(0, 1)

    if degree_as_tag:
        for g in g_list:
            g.node_tags = list(dict(g.g.degree).values())

    # Extracting unique tag labels
    tagset = set([])
    for g in g_list:
        tagset = tagset.union(set(g.node_tags))

    tagset = list(tagset)
    tag2index = {tagset[i]: i for i in range(len(tagset))}

    for g in g_list:
        g.node_features = torch.zeros(len(g.node_tags), len(tagset))
        g.node_features[range(len(g.node_tags)), [tag2index[tag] for tag in g.node_tags]] = 1

    print('# classes: %d' % len(label_dict))
    print('# maximum node tag: %d' % len(tagset))

    print("# data: %d" % len(g_list))

    networkx_graph_list = []
    node_features_list = []
    for g in g_list:
        networkx_graph_list.append(g.g)
        node_features_list.append(np.float64(g.node_features.numpy()))
    return networkx_graph_list, node_features_list


# load ENZYMES and PROTEIN and DD dataset
def Graph_load_batch(min_num_nodes=20, max_num_nodes=1000, name='ENZYMES', node_attributes=True, graph_labels=True):
    '''
    load many graphs, e.g. enzymes
    :return: a list of graphs
    '''
    print('Loading graph dataset: ' + str(name))
    G = nx.Graph()
    # load data
    path = '../dataset/' + name + '/'
    data_adj = np.loadtxt(path + name + '_A.txt', delimiter=',').astype(int)
    if node_attributes:
        data_node_att = np.loadtxt(path + name + '_node_attributes.txt', delimiter=',')
    data_node_label = np.loadtxt(path + name + '_node_labels.txt', delimiter=',').astype(int)
    data_graph_indicator = np.loadtxt(path + name + '_graph_indicator.txt', delimiter=',').astype(int)
    if graph_labels:
        data_graph_labels = np.loadtxt(path + name + '_graph_labels.txt', delimiter=',').astype(int)

    data_tuple = list(map(tuple, data_adj))
    # print(len(data_tuple))
    # print(data_tuple[0])

    # add edges
    G.add_edges_from(data_tuple)
    # add node attributes
    for i in range(data_node_label.shape[0]):
        if node_attributes:
            G.add_node(i + 1, feature=data_node_att[i])
        G.add_node(i + 1, label=data_node_label[i])
    G.remove_nodes_from(list(nx.isolates(G)))

    # print(G.number_of_nodes())
    # print(G.number_of_edges())

    # split into graphs
    graph_num = data_graph_indicator.max()
    node_list = np.arange(data_graph_indicator.shape[0]) + 1
    graphs = []
    max_nodes = 0
    for i in range(graph_num):
        # find the nodes for each graph
        nodes = node_list[data_graph_indicator == i + 1]
        G_sub = G.subgraph(nodes)
        if graph_labels:
            G_sub.graph['label'] = data_graph_labels[i]
        # print('nodes', G_sub.number_of_nodes())
        # print('edges', G_sub.number_of_edges())
        # print('label', G_sub.graph)
        if G_sub.number_of_nodes() >= min_num_nodes and G_sub.number_of_nodes() <= max_num_nodes:
            graphs.append(G_sub)
            if G_sub.number_of_nodes() > max_nodes:
                max_nodes = G_sub.number_of_nodes()
            # print(G_sub.number_of_nodes(), 'i', i)
    # print('Graph dataset name: {}, total graph num: {}'.format(name, len(graphs)))
    # logging.warning('Graphs loaded, total num: {}'.format(len(graphs)))
    print('Loaded')
    return graphs


def save_graphs_as_mat(graphs_list):
    if os.path.isdir("sami_test_graphs"):
        shutil.rmtree("sami_test_graphs")
    if not os.path.exists('sami_test_graphs'):
        os.makedirs('sami_test_graphs')
    counter = 0
    for g in graphs_list:
        counter += 1
        curr_graph = nx.to_numpy_array(g)
        numpy_matrix = nx.to_numpy_matrix(g)

        # print(1.0 - (np.count_nonzero(numpy_matrix) / float(numpy_matrix.size)))
        if counter == 101:
            print(1.0 - (np.count_nonzero(numpy_matrix) / float(numpy_matrix.size)))
            print("###########################################################")
            print(curr_graph[0])
        curr_graph_att = np.ones((len(curr_graph), 60))
        scipy.io.savemat('sami_test_graphs/testgraph_{}_{}__.txt.mat'.format(curr_graph.shape[0], counter, g),
                         {'data': curr_graph})
        scipy.io.savemat('sami_test_graphs/testgraph_{}_{}__.usr.mat'.format(curr_graph.shape[0], counter, g),
                         {'attributes': curr_graph_att})


def move_random_node_to_the_last_index(adj):
    # selecting a random node and moving it to the last node position
    random_idx_for_delete = np.random.randint(adj.shape[0])
    deleted_node = adj[:, random_idx_for_delete].copy()
    for i in range(deleted_node.__len__()):
        if i >= random_idx_for_delete and i < deleted_node.__len__() - 1:
            deleted_node[i] = deleted_node[i + 1]
        elif i == deleted_node.__len__() - 1:
            deleted_node[i] = 0
    adj[:, random_idx_for_delete:adj.shape[0] - 1] = adj[:, random_idx_for_delete + 1:adj.shape[0]]
    adj[random_idx_for_delete:adj.shape[0] - 1, :] = adj[random_idx_for_delete + 1:adj.shape[0], :]
    adj = np.delete(adj, -1, axis=1)
    adj = np.delete(adj, -1, axis=0)
    adj = np.concatenate((adj, deleted_node[:deleted_node.shape[0] - 1]), axis=1)
    adj = np.concatenate((adj, np.transpose(deleted_node)), axis=0)
    return adj


def prepare_kronEM_data(graphs_list, data_name, random_node_permutation_flag):
    if os.path.isdir("kronEM_main_graphs_" + data_name):
        shutil.rmtree("kronEM_main_graphs_" + data_name)
    if not os.path.exists("kronEM_main_graphs_" + data_name):
        os.makedirs("kronEM_main_graphs_" + data_name)
    if os.path.isdir("kronEM_graphs_with_missing_node_" + data_name):
        shutil.rmtree("kronEM_graphs_with_missing_node_" + data_name)
    if not os.path.exists("kronEM_graphs_with_missing_node_" + data_name):
        os.makedirs("kronEM_graphs_with_missing_node_" + data_name)
    counter = 0
    if random_node_permutation_flag:
        number_of_random_node_permutation_per_graph = 3
    else:
        number_of_random_node_permutation_per_graph = 1
    for g in graphs_list:
        for i in range(number_of_random_node_permutation_per_graph):
            counter += 1
            numpy_matrix = nx.to_numpy_matrix(g)
            if random_node_permutation_flag:
                numpy_matrix = move_random_node_to_the_last_index(numpy_matrix)

            file_main = open("kronEM_main_graphs_" + data_name + "/" + str(counter) + ".txt", "w")
            file_missing = open("kronEM_graphs_with_missing_node_" + data_name + "/" + str(counter) + ".txt", "w")
            with file_main as f:
                for i in range(numpy_matrix.shape[0]):
                    for j in range(numpy_matrix.shape[0]):
                        if numpy_matrix[i, j] == 1:
                            f.write(str(i + 1) + "\t" + str(j + 1) + "\n")
                            if i != numpy_matrix.shape[0] - 1 and j != numpy_matrix.shape[0] - 1:
                                file_missing.write(str(i + 1) + "\t" + str(j + 1) + "\n")
            file_main.close()
    return


def precision_recall_curve(ground_truth, probabilistic_prediction, just_get_threshold=False, dataset="", method_name="",
                           out_path='./precision_recall'):
    precision, recall, thresholds = metrics.precision_recall_curve(ground_truth, probabilistic_prediction)
    f_measure = np.nan_to_num(2 * recall * precision / (recall + precision))
    index = (np.where(f_measure == np.amax(f_measure)))[0].tolist()[0]
    if just_get_threshold:
        return thresholds[index]
    print("Max Value, precision: %.2f%%,recall: %.2f%%, f-score: %0.2f%%, threshold: %0.5f" % (
        precision[index] * 100, recall[index] * 100, f_measure[index] * 100, thresholds[index]))
    # Save the raw precision and recall results to a pickle since we might want
    # to analyse them later.
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    out_file = os.path.join(out_path, "precision_recall.pkl")
    with open(out_file, "wb") as out:
        pickle.dump({
            "precision": precision,
            "recall": recall,
            "thresholds": thresholds
        }, out)

    # Create the precision-recall curve.
    out_file = os.path.join(out_path, "precision_recall_" + dataset + '_' + method_name + ".png")
    plt.clf()
    plt.plot(recall, precision, label="Precision-Recall curve")
    plt.title('Dataset: ' + dataset + " - Method Name: " + method_name)
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    plt.savefig(out_file)


def plot_grad_flow(named_parameters, model_name):
    ave_grads = []
    layers = []
    for n, p in named_parameters:
        if (p.requires_grad) and ("bias" not in n) and p.grad != None:
            layers.append(n)
            ave_grads.append(p.grad.abs().mean())
    plt.plot(ave_grads, alpha=0.3, linewidth=1.0, color="r")
    plt.hlines(0, 0, len(ave_grads) + 1, linewidth=1, color="k")
    plt.xticks(range(0, len(ave_grads), 1), layers, fontsize=3, rotation=50)
    plt.xlim(xmin=0, xmax=len(ave_grads))
    plt.xlabel("Layers")
    plt.ylabel("average gradient")
    plt.title("Gradient flow")
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(model_name + ".pdf")


def move_random_node_to_the_last_index(adj, number_of_nodes):
    # selecting a random node and moving it to the last node position
    adj = np.asmatrix(adj)
    padding_size = adj.shape[0] - number_of_nodes
    random_idx = np.random.randint(number_of_nodes)
    selected_node = adj[:number_of_nodes, random_idx].copy()
    for i in range(selected_node.__len__()):
        if i >= random_idx and i < selected_node.__len__() - 1:
            selected_node[i] = selected_node[i + 1]
        elif i == selected_node.__len__() - 1:
            selected_node[i] = 0
    adj[:, random_idx:number_of_nodes - 1] = adj[:, random_idx + 1:number_of_nodes]
    adj[random_idx:number_of_nodes - 1, :] = adj[random_idx + 1:number_of_nodes, :]
    adj = adj[:, :-padding_size - 1]
    adj = adj[:-padding_size - 1, :]
    adj = np.concatenate((adj, selected_node[:selected_node.shape[0] - 1]), axis=1)
    adj = np.concatenate((adj, np.transpose(selected_node)), axis=0)
    adj_padded = np.zeros((number_of_nodes + padding_size, number_of_nodes + padding_size))
    adj_padded[:number_of_nodes, :number_of_nodes] = adj
    return np.asarray(adj_padded)


def save_graphs_as_zip(args, graph_list, path):
    zip_file_name = path
    path += '/'
    if os.path.isdir(path):
        files = glob.glob(path + '*')
        for f in files:
            os.remove(f)
    else:
        os.makedirs(path)
    for i, graph in enumerate(graph_list):
        if args.dataset == 'DD':
            # DD graphs have a lot of information and this makes it too much time consuming to save them
            adj = nx.adjacency_matrix(graph)
            graph = nx.from_numpy_matrix(adj.A)
        nx.write_gpickle(graph, path + str(i) + ".gpickle")
    shutil.make_archive(zip_file_name, 'zip', path)
    return


def save_original_and_incomplete_graphs(args, original_graphs, incomplete_graphs, root_path='./test_data/'):
    if not os.path.isdir(root_path):
        os.makedirs(root_path)
    path = root_path + args.dataset + '_# missing = ' + str(args.number_of_missing_nodes)
    save_graphs_as_zip(args, original_graphs, path)
    path = root_path + args.dataset + '_# missing = ' + str(args.number_of_missing_nodes) + '_incomplete'
    save_graphs_as_zip(args, incomplete_graphs, path)
    return


def save_generated_graphs(args, generated_graphs, root_path='./test_data/'):
    if not os.path.isdir(root_path):
        os.makedirs(root_path)
    path = root_path + args.dataset + '_# missing = ' + str(args.number_of_missing_nodes) + '_generated_graphs'
    save_graphs_as_zip(args, generated_graphs, path)
    return


def check_data_validity(number_of_missing_nodes, original_graphs, incomplete_graphs, generated_graphs):
    if len(original_graphs) != len(incomplete_graphs) or len(original_graphs) != len(generated_graphs):
        print("Number of graphs are not equal!")
        return False
    else:
        for i in range(len(original_graphs)):
            if original_graphs[i].number_of_nodes() != generated_graphs[i].number_of_nodes() or original_graphs[
                i].number_of_nodes() - number_of_missing_nodes != incomplete_graphs[i].number_of_nodes():
                print("Number of graph nodes does not match!")
                return False
    return True


def get_small_graphs(graphs, max_num_nodes):
    small_graphs = [g for g in graphs if g.number_of_nodes() <= max_num_nodes]
    max_num_nodes = max([small_graphs[i].number_of_nodes() for i in range(len(small_graphs))])
    return small_graphs, max_num_nodes


def remove_last_n_nodes(args, n, num_nodes, numpy_adj, adj_input):
    update_index = num_nodes - n
    label = adj_input[:update_index, update_index].copy()
    all_missing_nodes_labels = np.zeros((args.max_num_nodes - n, n))  # it is used when args.dsgc_rnn_mode = True
    all_missing_nodes_labels[:update_index, :] = adj_input[:update_index, update_index:num_nodes].copy()
    if args.connect_missing_node_to_all_existing_nodes:
        link_integer = 1
    else:
        link_integer = 0
    numpy_adj[:update_index + 1, update_index] = link_integer
    numpy_adj[update_index, :update_index + 1] = link_integer
    numpy_adj[:, update_index + 1:num_nodes] = 0
    numpy_adj[update_index + 1:num_nodes, :] = 0
    num_nodes = update_index + 1
    return label, all_missing_nodes_labels, num_nodes, update_index


def remove_links_between_missing_nodes(number_of_missing_nodes, batch_num_nodes, adj):
    batch_size = adj.shape[0]
    adj_without_links_between_missing_nodes = adj.copy()
    links_between_missing_nodes = np.zeros((batch_size, number_of_missing_nodes, number_of_missing_nodes))
    for i in range(batch_size):
        num_nodes = batch_num_nodes[i]
        update_index = num_nodes - number_of_missing_nodes
        links_between_missing_nodes[i] = adj[i, update_index:num_nodes, update_index:num_nodes].copy()
        adj_without_links_between_missing_nodes[i, update_index:num_nodes, update_index:num_nodes] = 0
    return adj_without_links_between_missing_nodes, links_between_missing_nodes


def remove_arbitrary_node(numpy_adj, num_nodes):
    # use this function only when args.number_of_missing_nodes = 1 and args.arbitrary_node = True
    # and args.transformer = False
    # this function will probably be removed in the future
    random_index = np.random.randint(num_nodes)
    true_label = list(numpy_adj[:random_index, random_index].copy()) + \
                 list(numpy_adj[random_index + 1:num_nodes, random_index].copy())
    numpy_adj[:num_nodes, random_index] = 1
    numpy_adj[random_index, :num_nodes] = 1
    return true_label, numpy_adj, random_index
