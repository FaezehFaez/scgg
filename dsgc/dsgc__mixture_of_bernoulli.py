import torch.nn as nn


class MixtureBernoulli(nn.Module):
    def __init__(self, config):
        super(MixtureBernoulli, self).__init__()
        self.config = config
        self.device = config.device
        self.max_num_nodes = config.model.max_num_nodes
        self.hidden_dim = config.model.hidden_dim
        self.num_mix_component = config.model.num_mix_component

        self.mlp_theta = nn.Sequential(
            nn.Linear(self.hidden_dim, self.hidden_dim),
            nn.ReLU(inplace=True),
            nn.Linear(self.hidden_dim, self.hidden_dim),
            nn.ReLU(inplace=True),
            nn.Linear(self.hidden_dim, self.self.num_mix_component))

        self.mlp_alpha = nn.Sequential(
            nn.Linear(self.hidden_dim, self.hidden_dim),
            nn.ReLU(inplace=True),
            nn.Linear(self.hidden_dim, self.hidden_dim),
            nn.ReLU(inplace=True),
            nn.Linear(self.hidden_dim, self.num_mix_component))

        ### Loss functions
        pos_weight = torch.ones([1]) * self.edge_weight
        self.adj_loss_func = nn.BCEWithLogitsLoss(
            pos_weight=pos_weight, reduction='none')

    def mixture_bernoulli_loss(label, log_theta, log_alpha, adj_loss_func,
                               subgraph_idx, subgraph_idx_base, num_canonical_order):
        num_subgraph = subgraph_idx_base[-1]  # == subgraph_idx.max() + 1
        K = log_theta.shape[1]

        adj_loss = torch.stack(
            [adj_loss_func(log_theta[:, kk], label) for kk in range(K)], dim=1)

        const = torch.zeros(num_subgraph).to(label.device)  # S
        const = const.scatter_add(0, subgraph_idx,
                                  torch.ones_like(subgraph_idx).float())

        reduce_adj_loss = torch.zeros(num_subgraph, K).to(label.device)
        reduce_adj_loss = reduce_adj_loss.scatter_add(
            0, subgraph_idx.unsqueeze(1).expand(-1, K), adj_loss)

        reduce_log_alpha = torch.zeros(num_subgraph, K).to(label.device)
        reduce_log_alpha = reduce_log_alpha.scatter_add(
            0, subgraph_idx.unsqueeze(1).expand(-1, K), log_alpha)
        reduce_log_alpha = reduce_log_alpha / const.view(-1, 1)
        reduce_log_alpha = F.log_softmax(reduce_log_alpha, -1)

        log_prob = -reduce_adj_loss + reduce_log_alpha
        log_prob = torch.logsumexp(log_prob, dim=1)  # S, K
        loss = -log_prob.sum() / float(log_theta.shape[0])

        return loss
