from random import shuffle

import networkx as nx
from networkx.algorithms.community import LFR_benchmark_graph

import utils.data as data
from dsgc.dsgc_util import get_small_graphs, load_data, Graph_load_batch


def create(args):
    graphs = []
    node_features = []
    max_num_nodes = -1
    if args.dataset == 'enzymes':
        graphs = data.Graph_load_batch(min_num_nodes=10, name='ENZYMES')
        graphs = [g for g in graphs if g.number_of_nodes() >= args.min_graph_size]
    elif args.dataset == 'enzymes-50':
        graphs = data.Graph_load_batch(min_num_nodes=10, name='ENZYMES')
        graphs, max_num_nodes = get_small_graphs(graphs, max_num_nodes=50)
    elif args.dataset == 'enzymes-100':
        graphs = data.Graph_load_batch(min_num_nodes=10, name='ENZYMES')
        graphs, max_num_nodes = get_small_graphs(graphs, max_num_nodes=100)
    elif args.dataset == 'pubmed':
        _, _, G = data.Graph_load(dataset='pubmed')
        G = max(nx.connected_component_subgraphs(G), key=len)
        G = nx.convert_node_labels_to_integers(G)
        graphs = []
        for i in range(G.number_of_nodes()):
            G_ego = nx.ego_graph(G, i, radius=3)
            if G_ego.number_of_nodes() <= args.prune_graph_size:
                graphs.append(G_ego)
    elif args.dataset == 'cora':
        _, _, G = data.Graph_load(dataset='cora')
        G = max(nx.connected_component_subgraphs(G), key=len)
        G = nx.convert_node_labels_to_integers(G)
        graphs = []
        for i in range(G.number_of_nodes()):
            G_ego = nx.ego_graph(G, i, radius=3)
            if G_ego.number_of_nodes() >= 50 and (G_ego.number_of_nodes() <= 400):
                graphs.append(G_ego)
    elif args.dataset == 'cora_small':
        _, _, G = data.Graph_load(dataset='cora')
        G = max(nx.connected_component_subgraphs(G), key=len)
        G = nx.convert_node_labels_to_integers(G)
        graphs = []
        for i in range(G.number_of_nodes()):
            G_ego = nx.ego_graph(G, i, radius=1)
            if (G_ego.number_of_nodes() >= 4) and (G_ego.number_of_nodes() <= 20):
                graphs.append(G_ego)
        shuffle(graphs)
    elif args.dataset == 'citeseer':
        _, _, G = data.Graph_load(dataset='citeseer')
        G = max(nx.connected_component_subgraphs(G), key=len)
        G = nx.convert_node_labels_to_integers(G)
        graphs = []
        for i in range(G.number_of_nodes()):
            G_ego = nx.ego_graph(G, i, radius=3)
            if G_ego.number_of_nodes() >= 50 and (G_ego.number_of_nodes() <= 400):
                graphs.append(G_ego)
    elif args.dataset == 'citeseer_small':
        _, _, G = data.Graph_load(dataset='citeseer')
        G = max(nx.connected_component_subgraphs(G), key=len)
        G = nx.convert_node_labels_to_integers(G)
        graphs = []
        for i in range(G.number_of_nodes()):
            G_ego = nx.ego_graph(G, i, radius=1)
            if (G_ego.number_of_nodes() >= 4) and (G_ego.number_of_nodes() <= 20):
                graphs.append(G_ego)
        shuffle(graphs)
        # graphs = graphs[0:200]
    elif args.dataset == 'lfr':
        n = 200
        tau1 = 3
        tau2 = 1.5
        mu = 0.1
        number_of_graphs = 100
        for i in range(number_of_graphs):
            G = LFR_benchmark_graph(n, tau1, tau2, mu, average_degree=5, min_community=20, seed=10)
            graphs.append(G)
    elif args.dataset == 'barabasi':
        graphs = []
        for i in range(100, 200):
            for j in range(4, 5):
                for k in range(5):
                    graphs.append(nx.barabasi_albert_graph(i, j))
    elif args.dataset == 'barabasi_small':
        graphs = []
        for i in range(4, 21):
            for j in range(3, 4):
                for k in range(10):
                    graphs.append(nx.barabasi_albert_graph(i, j))
    elif args.dataset == 'grid':
        graphs = []
        for i in range(5, 20):
            for j in range(5, 20):
                graphs.append(nx.grid_2d_graph(i, j))
    elif args.dataset == 'grid_small':
        graphs = []
        for i in range(2, 5):
            for j in range(2, 6):
                graphs.append(nx.grid_2d_graph(i, j))
    elif args.dataset == 'DD':
        graphs = Graph_load_batch(min_num_nodes=100, max_num_nodes=500, name='DD',node_attributes=False,graph_labels=True)
    else:
        graphs, node_features = load_data(args.dataset, args.degree_as_feature, args.prune_graph_size, args.min_graph_size)
        args.node_features_available = True
        args.node_features_dim = node_features[0].shape[1]
        if args.prune_graph_size != -1:
            max_num_nodes = max([graphs[i].number_of_nodes() for i in range(len(graphs))])
        args.dataset = args.dataset + '_' + str(args.min_graph_size) + '_' + str(args.prune_graph_size)
    return graphs, node_features, args.dataset, max_num_nodes
