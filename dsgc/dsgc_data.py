import networkx as nx
import numpy as np
import torch

from utils.data import bfs_seq


class GraphAdjSampler(torch.utils.data.Dataset):
    def __init__(self, G_list, node_features_list, max_num_nodes, args):
        self.max_num_nodes = max_num_nodes
        self.adj_all = []
        self.feature_all = []
        self.permutation_mode = args.permutation_mode
        self.bfs_mode = args.bfs_mode
        self.bfs_mode_with_arbitrary_node_deleted = args.bfs_mode_with_arbitrary_node_deleted

        if args.node_features_available and args.use_node_features:
            self.feature_all = node_features_list
            args.feature_type = 'feature'  # features variable is not anymore 'id', 'deg', or 'struct'

        for G in G_list:
            adj = nx.to_numpy_matrix(G)
            # the diagonal entries are 1 since they denote node probability
            # self.adj_all.append(np.asarray(adj) + np.identity(G.number_of_nodes()))
            self.adj_all.append(np.asarray(adj))
            if args.feature_type == 'id':
                self.feature_all.append(np.identity(max_num_nodes))
            elif args.feature_type == 'deg':
                degs = np.sum(np.array(adj), 1)
                degs = np.expand_dims(np.pad(degs, [0, max_num_nodes - G.number_of_nodes()], 0),
                                      axis=1)
                self.feature_all.append(degs)
            elif args.feature_type == 'struct':
                degs = np.sum(np.array(adj), 1)
                degs = np.expand_dims(np.pad(degs, [0, max_num_nodes - G.number_of_nodes()],
                                             'constant'),
                                      axis=1)
                clusterings = np.array(list(nx.clustering(G).values()))
                clusterings = np.expand_dims(np.pad(clusterings,
                                                    [0, max_num_nodes - G.number_of_nodes()],
                                                    'constant'),
                                             axis=1)
                self.feature_all.append(np.hstack([degs, clusterings]))

    def __len__(self):
        return len(self.adj_all)

    def __getitem__(self, idx):
        adj = self.adj_all[idx].copy()
        feature = self.feature_all[idx].copy()
        if self.permutation_mode:
            x_idx = np.random.permutation(adj.shape[0])
            adj = adj[np.ix_(x_idx, x_idx)]
            adj = np.asmatrix(adj)
            if self.bfs_mode:
                if self.bfs_mode_with_arbitrary_node_deleted:
                    random_idx_for_delete = np.random.randint(adj.shape[0])
                    deleted_node = adj[:, random_idx_for_delete].copy()
                    for i in range(deleted_node.__len__()):
                        if i >= random_idx_for_delete and i < deleted_node.__len__() - 1:
                            deleted_node[i] = deleted_node[i + 1]
                        elif i == deleted_node.__len__() - 1:
                            deleted_node[i] = 0
                    adj[:, random_idx_for_delete:adj.shape[0] - 1] = adj[:, random_idx_for_delete + 1:adj.shape[0]]
                    adj[random_idx_for_delete:adj.shape[0] - 1, :] = adj[random_idx_for_delete + 1:adj.shape[0], :]
                    adj = np.delete(adj, -1, axis=1)
                    adj = np.delete(adj, -1, axis=0)
                    G = nx.from_numpy_matrix(adj)
                    # then do bfs in the permuted G
                    degree_arr = np.sum(adj, axis=0)
                    start_idx = np.argmax(degree_arr)
                    # start_idx = np.random.randint(adj.shape[0])
                    x_idx = np.array(bfs_seq(G, start_idx))
                    adj = adj[np.ix_(x_idx, x_idx)]
                    x_idx = np.insert(x_idx, x_idx.size, x_idx.size)
                    deleted_node = deleted_node[np.ix_(x_idx)]
                    adj = np.append(adj, deleted_node[:-1], axis=1)
                    deleted_node = deleted_node.reshape(1, -1)
                    adj = np.vstack([adj, deleted_node])
                else:
                    G = nx.from_numpy_matrix(adj)
                    # then do bfs in the permuted G
                    start_idx = np.random.randint(adj.shape[0])
                    x_idx = np.array(bfs_seq(G, start_idx))
                    adj = adj[np.ix_(x_idx, x_idx)]
        num_nodes = adj.shape[0]
        adj_padded = np.zeros((self.max_num_nodes, self.max_num_nodes))
        adj_padded[:num_nodes, :num_nodes] = adj
        feature_padded = np.zeros((self.max_num_nodes, feature.shape[1]))
        feature_padded[:num_nodes, :] = feature[:num_nodes, :]
        return {'adj': adj_padded,
                'num_nodes': num_nodes,
                'features': feature_padded}
