import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init

import utils.model as model
from dsgc.dsgc_util import move_random_node_to_the_last_index, remove_arbitrary_node, remove_last_n_nodes


class DSGC(nn.Module):
    def __init__(self, input_dim, hidden_dim, max_num_nodes, args):
        '''
        Args:
            input_dim: input feature dimension for node.
            hidden_dim: hidden dim for 2-layer gcn.
            latent_dim: dimension of the latent representation of graph.
        '''
        self.args = args
        super(DSGC, self).__init__()
        self.hidden_dim = hidden_dim
        if self.args.basic_graph_conv:
            self.conv1 = model.BasicGraphConv(input_dim=input_dim, output_dim=self.args.intermediate_dim)
            self.conv2 = model.BasicGraphConv(input_dim=self.args.intermediate_dim, output_dim=hidden_dim)
        else:
            self.conv1 = model.GraphConv(input_dim=input_dim, output_dim=self.args.intermediate_dim,
                                         add_self=self.args.graph_conv_add_self,
                                         normalize_embedding=self.args.graph_conv_normalize_embedding,
                                         dropout=self.args.dropout, bias=self.args.graph_conv_bias)
            self.conv2 = model.GraphConv(input_dim=self.args.intermediate_dim, output_dim=hidden_dim,
                                         add_self=self.args.graph_conv_add_self,
                                         normalize_embedding=self.args.graph_conv_normalize_embedding,
                                         dropout=self.args.dropout, bias=self.args.graph_conv_bias)

        self.bn = nn.BatchNorm1d(max_num_nodes)
        self.act = nn.ReLU()

        # Transformer Encoder
        if self.args.transformer_after_gcn:
            self.encoder_layer = nn.TransformerEncoderLayer(d_model=hidden_dim,
                                                            nhead=self.args.number_of_attention_heads,
                                                            dropout=self.args.transformer_dropout)
        else:
            self.encoder_layer = nn.TransformerEncoderLayer(d_model=max_num_nodes,
                                                            nhead=self.args.number_of_attention_heads,
                                                            dropout=self.args.transformer_dropout)
        self.transformer_encoder = nn.TransformerEncoder(self.encoder_layer,
                                                         num_layers=self.args.number_of_transformer_encoder_layers)
        if self.args.transformer:  # it maybe need to be improved in the future
            self.args.node_feature_dim = 2 * self.hidden_dim
        else:
            self.args.node_feature_dim = self.hidden_dim

        output_dim = max_num_nodes * (max_num_nodes + 1) // 2

        self.vae = model.MLP_VAE_plain(input_dim * hidden_dim, hidden_dim, output_dim)

        self.max_num_nodes = max_num_nodes
        self.gran = model.GRAN(self.max_num_nodes, self.args.transformer, self.args.transformer_after_gcn,
                               self.hidden_dim, self.args.dropout)
        for m in self.modules():
            if isinstance(m, model.BasicGraphConv) or isinstance(m, model.GraphConv):
                m.weight.data = init.xavier_uniform(m.weight.data, gain=nn.init.calculate_gain('relu'))
                if isinstance(m, model.GraphConv) and self.args.graph_conv_bias:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm1d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.TransformerEncoderLayer):
                for p in m.parameters():
                    if p.dim() > 1:
                        init.xavier_uniform_(p)

    def prepare_data(self, adj, batch_num_nodes):
        numpy_adj = adj.cpu().numpy()
        batch_size = numpy_adj.shape[0]
        true_label = np.zeros((batch_size, self.max_num_nodes - 1))
        all_missing_nodes_labels = np.zeros((batch_size, self.args.max_num_nodes - self.args.number_of_missing_nodes,
                                             self.args.number_of_missing_nodes))
        random_index_list = []
        for i in range(batch_size):
            num_nodes = batch_num_nodes[i]
            if self.args.number_of_missing_nodes == 1 and self.args.arbitrary_node and not self.args.transformer:
                label, numpy_adj[i], random_index = remove_arbitrary_node(numpy_adj[i], num_nodes)
                true_label[i, :num_nodes - 1] = label
                random_index_list.append(random_index)
            else:
                if self.args.arbitrary_node and self.args.transformer:
                    numpy_adj[i] = move_random_node_to_the_last_index(numpy_adj[i], num_nodes)
                if self.args.dsgc_rnn_mode:
                    number_of_missing = self.args.number_of_missing_nodes
                    label, all_missing_nodes_labels[i], num_nodes, update_index = \
                        remove_last_n_nodes(self.args, number_of_missing, num_nodes, numpy_adj[i], numpy_adj[i])
                else:
                    number_of_missing = np.random.randint(self.args.number_of_missing_nodes) + 1
                    label, _, num_nodes, update_index = \
                        remove_last_n_nodes(self.args, number_of_missing, num_nodes, numpy_adj[i], numpy_adj[i])
                true_label[i, :update_index] = label
                batch_num_nodes[i] = num_nodes
        true_label = torch.tensor(true_label).float()
        incomplete_adj = torch.tensor(numpy_adj).cuda()
        all_missing_nodes_labels = torch.tensor(all_missing_nodes_labels).float()
        return true_label, all_missing_nodes_labels, incomplete_adj, batch_size, random_index_list, batch_num_nodes

    def graph_embedder(self, input_features, incomplete_adj, pygcn_model=None):
        concatenated_transformer_features = 0  # empty initialization
        if pygcn_model is None:
            gnn_features = self.conv1(input_features, incomplete_adj)
            gnn_features = self.act(gnn_features)
            gnn_features = self.bn(gnn_features)
            gnn_features = self.conv2(gnn_features, incomplete_adj)
        else:
            gnn_features = pygcn_model(input_features, incomplete_adj)
        if self.args.transformer:
            if self.args.transformer_after_gcn:
                self_attended_features = self.transformer_encoder(gnn_features)
            else:
                self_attended_features = self.transformer_encoder(incomplete_adj)
            concatenated_transformer_features = torch.cat((gnn_features, self_attended_features), 2)
        return gnn_features, concatenated_transformer_features

    def forward(self, input_features, adj, batch_num_nodes):
        true_label, _, incomplete_adj, batch_size, random_index_list, batch_num_nodes = self.prepare_data(adj,
                                                                                                          batch_num_nodes)

        pos_weight = torch.ones(batch_size)
        for i in range(batch_size):
            pos_weight[i] = torch.Tensor(
                [float(adj[i].shape[0] * adj[i].shape[0] - adj[i].sum()) / adj[i].sum()])
        pos_weight = pos_weight.unsqueeze(1)
        ################################################################### embedding
        gnn_features, concatenated_transformer_features = self.graph_embedder(input_features, incomplete_adj)
        if self.args.transformer:
            loss = F.binary_cross_entropy_with_logits(
                self.gran.output_theta(concatenated_transformer_features[:, :-1, :]).squeeze().cpu(), true_label,
                pos_weight=pos_weight)
        else:
            loss = F.binary_cross_entropy_with_logits(
                self.gran(gnn_features, batch_num_nodes, random_index_list, self.args.arbitrary_node).squeeze().cpu(),
                true_label, pos_weight=pos_weight)
        return loss
