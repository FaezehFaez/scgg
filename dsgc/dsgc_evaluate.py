import random

import networkx as nx
import numpy as np
import torch
from sklearn.metrics import roc_auc_score, average_precision_score

from dsgc.dsgc_util import precision_recall_curve, move_random_node_to_the_last_index, remove_last_n_nodes, \
    remove_arbitrary_node
from utils.evaluation import compute_evaluation_metrics
from utils.model import sample_sigmoid
from utils.utils import get_graph


def getitem(args, graph, feature, max_num_nodes, number_of_missing_nodes):
    if feature is None or not args.use_node_features:
        if args.feature_type == 'id':
            feature = np.identity(max_num_nodes)
    adj = nx.to_numpy_matrix(graph)
    # adj = adj + np.identity(graph.number_of_nodes())
    
    # if number_of_missing_nodes > 1:
    # random node permutation
    x_idx = np.random.permutation(adj.shape[0])
    adj = adj[np.ix_(x_idx, x_idx)]
    adj = np.asmatrix(adj)

    num_nodes = adj.shape[0]
    adj_padded = np.zeros((max_num_nodes, max_num_nodes))
    adj_padded[:num_nodes, :num_nodes] = adj
    feature_padded = np.zeros((max_num_nodes, feature.shape[1]))
    feature_padded[:num_nodes, :] = feature[:num_nodes, :]
    return {'adj': adj_padded,
            'num_nodes': num_nodes,
            'features': feature_padded}


def next_node_predictor(model, args, step_label, current_node_index, input_features, incomplete_adj, num_nodes,
                        random_index=0):
    incomplete_adj = torch.tensor(incomplete_adj).cuda().unsqueeze(0).float()
    x = model.conv1(input_features, incomplete_adj)
    x = model.act(x)
    x = model.bn(x)
    x = model.conv2(x, incomplete_adj)
    num_nodes = torch.tensor(num_nodes).cuda().unsqueeze(0)
    #
    if model.args.transformer:
        if model.args.transformer_after_gcn:
            self_attended_features = model.transformer_encoder(x)
        else:
            self_attended_features = model.transformer_encoder(incomplete_adj)
        concatenated_features = torch.cat((x, self_attended_features), 2)
        preds = model.gran.output_theta(concatenated_features[:, :-1, :]).squeeze()
    else:
        preds = model.gran(x, num_nodes, [random_index], model.args.arbitrary_node).squeeze()
    #
    probabilistic_y_pred = torch.sigmoid(preds)
    probabilistic_pred = probabilistic_y_pred.squeeze(0).squeeze(0).cpu().detach().numpy()[
                         :current_node_index]
    if args.automatic_threshold_for_testing:
        threshold = precision_recall_curve(step_label, probabilistic_pred, just_get_threshold=True)
        sample = sample_sigmoid(preds.unsqueeze(0).unsqueeze(0), sample=False, thresh=threshold,
                                sample_time=10)
    else:
        sample = sample_sigmoid(preds.unsqueeze(0).unsqueeze(0), sample=False, thresh=0.5, sample_time=10)
    sample = sample.squeeze().cpu().numpy()[:current_node_index]
    return sample, probabilistic_y_pred


def get_ap_roc(label, probabilistic_preds):
    positive = probabilistic_preds[label == 1]
    if len(positive) <= len(list(probabilistic_preds[label == 0])):
        negative = random.sample(list(probabilistic_preds[label == 0]), len(positive))
    else:
        negative = probabilistic_preds[label == 0]
        positive = random.sample(list(probabilistic_preds[label == 1]), len(negative))
    preds_all = np.hstack([positive, negative])
    labels_all = np.hstack([np.ones(len(positive)), np.zeros(len(positive))])
    if len(labels_all) > 0:
        roc_score = roc_auc_score(labels_all, preds_all)
        ap_score = average_precision_score(labels_all, preds_all)
    return roc_score, ap_score


def add_new_node(args, numpy_adj, index, num_nodes, result):
    if args.arbitrary_node and not args.transformer:
        numpy_adj[:index, index] = result[:index]
        numpy_adj[index + 1:num_nodes, index] = result[index:]
        numpy_adj[index, :index] = result[:index]
        numpy_adj[index, index + 1:num_nodes] = result[index:]
    else:
        numpy_adj[:index, index] = result
        numpy_adj[index, :index] = result
    return


def generate_step(model, current_node_index, numpy_adj, adj_input, input_features, num_nodes, step_number_of_missing,
                  label, result, probabilistic_preds):
    if model.args.arbitrary_node and not model.args.transformer:
        step_label, numpy_adj, index = remove_arbitrary_node(numpy_adj, num_nodes)
    else:
        index = current_node_index
        if model.args.arbitrary_node and model.args.transformer:
            numpy_adj = move_random_node_to_the_last_index(numpy_adj, num_nodes)
            adj_input = numpy_adj.copy()
        step_label, _, _, _ = remove_last_n_nodes(model.args, step_number_of_missing, num_nodes, numpy_adj, adj_input)
    step_incomplete_graph = get_graph(numpy_adj)
    sample, pred = next_node_predictor(model, model.args, step_label, current_node_index, input_features,
                                       numpy_adj, current_node_index + 1, index)
    add_new_node(model.args, numpy_adj, index, num_nodes, sample)
    label = label + list(step_label)
    result = result + list(sample)
    probabilistic_preds = probabilistic_preds + list(pred[:current_node_index].cpu().detach().numpy())
    return label, result, probabilistic_preds, numpy_adj, step_incomplete_graph


def evaluate(test_graph_list, test_node_features_list, model, graph_show_mode=False):
    model.eval()

    graphs_gen = []
    incomplete_graphs = []

    all_labels = []
    all_results = []
    all_probabilistic_preds = []

    number_of_missing_nodes = model.args.number_of_missing_nodes
    for i in range(len(test_graph_list)):
        graph = test_graph_list[i]
        if model.args.node_features_available:
            feature = test_node_features_list[i]
        else:
            feature = None
        data = getitem(model.args, graph, feature, model.max_num_nodes, number_of_missing_nodes)
        input_features = data['features']
        input_features = torch.tensor(input_features).cuda().unsqueeze(0).float()
        adj_input = data['adj']
        num_nodes = data['num_nodes']
        numpy_adj = adj_input.copy()

        result = []
        label = []
        probabilistic_preds = []

        for i in range(number_of_missing_nodes):
            step_number_of_missing = number_of_missing_nodes - i
            current_node_index = num_nodes - step_number_of_missing
            label, result, probabilistic_preds, numpy_adj, step_incomplete_graph = \
                generate_step(model, current_node_index, numpy_adj, adj_input, input_features, num_nodes,
                              step_number_of_missing, label, result, probabilistic_preds)
            if i == 0:
                incomplete_graphs.append(step_incomplete_graph)

        np.fill_diagonal(numpy_adj, 0)
        completed_adj = numpy_adj[:num_nodes, :num_nodes]
        graphs_gen.append(nx.from_numpy_matrix(completed_adj))

        all_labels = all_labels + list(label)
        all_results = all_results + list(result)
        all_probabilistic_preds = all_probabilistic_preds + list(probabilistic_preds)

    all_labels = list(all_labels)
    all_results = list(all_results)
    all_probabilistic_preds = list(all_probabilistic_preds)

    mae, precision, recall, f1, roc_score, ap_score = compute_evaluation_metrics(all_labels, all_results,
                                                                                 all_probabilistic_preds)
    return mae, precision, recall, f1, roc_score, ap_score, all_labels, all_probabilistic_preds, graphs_gen, incomplete_graphs
