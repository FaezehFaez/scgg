### program configuration
class DSGC_Args():
    def __init__(self):
        self.dataset = 'MUTAG'
        self.prune_graph_size = -1  # -1 for no graph size limitation
        self.min_graph_size = -1
        self.feature_type = 'id'  # Feature used for encoder. Can be: id, deg, feature. When feature_type = 'feature' it
        # means that the node features exist and we have the authority to use them, i.e., both the
        # node_features_available and the use_node_features parameters must be True
        self.node_features_available = False  # dsgc_create_graphs.create() function makes this parameter True in
        # case of node features being available
        self.node_features_dim = -1  # this parameter will be initialized by dsgc_create_graphs.create() when
        # args.node_features_available = True
        self.use_node_features = False
        self.lr = 0.003  # it was previously 0.01
        self.batch_size = 32
        self.batch_ratio = 10
        self.num_workers = 4  # Number of workers to load data
        self.epoch = 100
        self.max_num_nodes = -1  # Predefined maximum number of nodes in train/test graphs. -1 if determined by training data.

        # pygcn model arguments
        self.pygcn = False
        self.pygcn_hidden1 = 16
        self.pygcn_hidden2 = 16
        self.pygcn_dropout = 0.5
        self.pygcn_lr = 0.01
        self.pygcn_weight_decay = 5e-4
        self.degree_as_feature = False  # degree_as_feature parameter plays its role when loading one of the
        # following 9 datasets: COLLAB, IMDBBINARY, IMDBMULTI, MUTAG, NCI1, PROTEINS, PTC, REDDITBINARY,
        # REDDITMULTI5K. Please note that according to the paper "HOW POWERFUL ARE GRAPH NEURAL NETWORKS?",
        # degree_as_feature must be True when loading COLLAB, IMDBBINARY, IMDBMULTI and for loading other datasets it
        # must be False

        # rnn model arguments
        self.dsgc_rnn_mode = True
        self.predict_links_between_missing_nodes = True
        self.milestones = [400, 1000]
        self.lr_rate = 0.3
        self.node_feature_dim = -1  # it must be initialize base on whether we use transformer or not
        if 'small' in self.dataset:
            self.parameter_shrink = 2
        else:
            self.parameter_shrink = 1
        self.hidden_size_rnn = int(128 / self.parameter_shrink)  # hidden size for main RNN
        self.embedding_size_rnn = int(64 / self.parameter_shrink)  # the size for LSTM input
        self.embedding_size_output = int(64 / self.parameter_shrink)  # the embedding size for output
        self.num_layers = 4
        self.epochs_log = 1

        self.number_of_test_iterations = 10
        ### if clean tensorboard
        self.clean_tensorboard = False
        ### Which CUDA GPU device is used for training
        self.cuda = 0
        self.seed = 1234

        self.number_of_missing_nodes = 5
        # if self.number_of_missing_nodes > 1 then: args.permutation_mode must be True and args.arbitrary_node must
        # be False, (in previous model, which is inspired by gran) args.connect_missing_node_to_all_existing_nodes
        # must be True, At the test time we construct the graph node by node
        self.permutation_mode = True
        self.bfs_mode = False
        self.bfs_mode_with_arbitrary_node_deleted = False
        self.arbitrary_node = False
        # transformer argument (currently only implemented for reconstructing the last node so the arbitrary_node must be False)
        self.transformer = True
        self.connect_missing_node_to_all_existing_nodes = not self.transformer  # it will probably change in the future
        self.transformer_after_gcn = True
        self.compute_mmd_each_test_iteration = False
        self.automatic_threshold_for_testing = True
        self.dropout = 0.1
        self.transformer_dropout = 0.1
        self.number_of_transformer_encoder_layers = 1
        self.number_of_attention_heads = 8

        # graph conv parameters
        self.intermediate_dim = 16
        # if self.basic_graph_conv = False, the graph convolutional layers will support the dropout
        self.basic_graph_conv = True
        self.graph_conv_add_self = False
        self.graph_conv_normalize_embedding = False
        self.graph_conv_bias = False

        self.sample_time = 2  # sample time in each time step, when validating

        ### output config
        self.dir_input = "./"
        self.model_save_path = self.dir_input + 'model_save/'  # only for nll evaluation
        self.tensorboard_path = self.dir_input + 'tensorboard/'
