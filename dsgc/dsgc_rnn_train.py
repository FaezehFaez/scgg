import numpy as np
import torch
import torch.nn.functional as F
from tensorboard_logger import log_value
from torch import optim
from torch.autograd import Variable
from torch.optim.lr_scheduler import MultiStepLR

from dsgc.dsgc_evaluate import getitem
from dsgc.dsgc_util import save_original_and_incomplete_graphs, remove_last_n_nodes, remove_links_between_missing_nodes, \
    check_data_validity, save_generated_graphs
from pygcn.utils import prepare_data as pygcn_prepare_data
from utils.evaluation import compute_ged, get_average_ged_std_for_test_iterations, mmd_evaluate
from utils.model import binary_cross_entropy_weight, sample_sigmoid
from utils.utils import get_graph


def temp_print_grad_values(model, model_name):
    print("*****************************************   " + model_name)
    basic_dsgc_model_counter = 0
    for name, param in model.named_parameters():
        if not (param.requires_grad):
            print("salam")
        if (param.requires_grad):
            if param.grad == None:
                print("@@@   None grad parameters")
            else:
                basic_dsgc_model_counter += 1
            print(name, param.data.size())
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^    " + model_name + "_counter = " + str(basic_dsgc_model_counter))


def train_mlp_epoch(epoch, args, basic_dsgc_model, pygcn_model, rnn, output, data_loader, optimizer_basic_dsgc_model,
                    optimizer_pygcn_model, optimizer_rnn, optimizer_output, scheduler_basic_dsgc_model, scheduler_rnn,
                    scheduler_output):
    basic_dsgc_model.train()
    if args.pygcn:
        pygcn_model.train()
    rnn.train()
    output.train()
    loss_sum = 0
    for batch_idx, data in enumerate(data_loader):
        basic_dsgc_model.zero_grad()
        if args.pygcn:
            optimizer_pygcn_model.zero_grad()
        rnn.zero_grad()
        output.zero_grad()

        features = data['features'].float()
        batch_num_nodes = data['num_nodes'].int().numpy()
        batch_num_nodes_unsorted = torch.from_numpy(np.array(batch_num_nodes.copy()))
        adj_input = data['adj'].float()

        adj_input = Variable(adj_input).cuda()
        _, all_missing_nodes_labels, incomplete_adj, _, _, _ = basic_dsgc_model.prepare_data(adj_input, batch_num_nodes)
        if args.pygcn:
            incomplete_adj, features = pygcn_prepare_data(incomplete_adj.cpu().numpy(), features.numpy())
            incomplete_adj = torch.from_numpy(incomplete_adj).cuda()
            features = torch.from_numpy(features)
        features = Variable(features).cuda()
        gnn_features, concatenated_transformer_features = basic_dsgc_model.graph_embedder(features, incomplete_adj,
                                                                                          pygcn_model=pygcn_model)
        if args.predict_links_between_missing_nodes:
            adj_without_links_between_missing_nodes, links_between_missing_nodes = remove_links_between_missing_nodes(
                args.number_of_missing_nodes, batch_num_nodes_unsorted.numpy(), adj_input.cpu().numpy())
            if args.pygcn:
                adj_without_links_between_missing_nodes, features = pygcn_prepare_data(
                    adj_without_links_between_missing_nodes, features.cpu().numpy())
                adj_without_links_between_missing_nodes = torch.from_numpy(
                    adj_without_links_between_missing_nodes).cuda()
                features = torch.from_numpy(features).cuda()
            adj_without_links_between_missing_nodes = torch.tensor(adj_without_links_between_missing_nodes).cuda()
            links_between_missing_nodes = torch.tensor(links_between_missing_nodes).float()
            gnn_features_with_missing, concatenated_transformer_features_with_missing = basic_dsgc_model.graph_embedder(
                features, adj_without_links_between_missing_nodes, pygcn_model=pygcn_model)
            batch_size = adj_input.size()[0]
            tmp_concatenated_transformer_features = Variable(
                torch.zeros(concatenated_transformer_features.size())).cuda()
            tmp_gnn_features = Variable(torch.zeros(gnn_features.size())).cuda()
            tmp_all_missing_nodes_labels = np.zeros((batch_size, args.max_num_nodes, args.number_of_missing_nodes))
            for i in range(batch_size):
                tmp_concatenated_transformer_features[i, :batch_num_nodes_unsorted[i], :] = torch.cat(
                    (concatenated_transformer_features[i, :batch_num_nodes_unsorted[i] - args.number_of_missing_nodes,
                     :],
                     concatenated_transformer_features_with_missing[i,
                     batch_num_nodes_unsorted[i] - args.number_of_missing_nodes:batch_num_nodes_unsorted[i], :]), 0)
                tmp_gnn_features[i, :batch_num_nodes_unsorted[i], :] = torch.cat(
                    (gnn_features[i, :batch_num_nodes_unsorted[i] - args.number_of_missing_nodes, :],
                     gnn_features_with_missing[i,
                     batch_num_nodes_unsorted[i] - args.number_of_missing_nodes:batch_num_nodes_unsorted[i], :]), 0)
                tmp_all_missing_nodes_labels[i, :batch_num_nodes_unsorted[i], :] = torch.cat(
                    (all_missing_nodes_labels[i, :batch_num_nodes_unsorted[i] - args.number_of_missing_nodes, :],
                     links_between_missing_nodes[i]), 0)
            concatenated_transformer_features = tmp_concatenated_transformer_features
            gnn_features = tmp_gnn_features
            all_missing_nodes_labels = torch.from_numpy(tmp_all_missing_nodes_labels).float()
            max_row_index = args.max_num_nodes
        else:
            max_row_index = args.max_num_nodes - args.number_of_missing_nodes
        gnn_features = gnn_features[:, :max_row_index, :]
        concatenated_transformer_features = concatenated_transformer_features[:, :max_row_index, :]
        x_unsorted = np.ones((args.batch_size, max_row_index, args.number_of_missing_nodes))
        x_unsorted[:, 1:, :] = all_missing_nodes_labels[:, :-1, :]
        x_unsorted = torch.from_numpy(np.array(x_unsorted))
        x_unsorted = torch.cat((x_unsorted, concatenated_transformer_features.cpu()), 2)
        y_unsorted = all_missing_nodes_labels
        if args.predict_links_between_missing_nodes:
            y_len_max = max(batch_num_nodes_unsorted)
        else:
            y_len_max = max(batch_num_nodes_unsorted) - args.number_of_missing_nodes
        x_unsorted = x_unsorted[:, 0:y_len_max, :]
        y_unsorted = y_unsorted[:, 0:y_len_max, :]
        # initialize lstm hidden state according to batch size
        rnn.hidden = rnn.init_hidden(batch_size=x_unsorted.size(0))
        # sort input
        if args.predict_links_between_missing_nodes:
            y_len, sort_index = torch.sort(batch_num_nodes_unsorted, 0, descending=True)
        else:
            y_len, sort_index = torch.sort(batch_num_nodes_unsorted - args.number_of_missing_nodes, 0, descending=True)
        y_len = y_len.numpy().tolist()
        x = torch.index_select(x_unsorted, 0, sort_index).float().cuda()
        y = torch.index_select(y_unsorted, 0, sort_index).cuda()

        h = rnn(x, pack=True, input_len=y_len)
        y_pred = output(h)
        y_pred = F.sigmoid(y_pred)
        # clean
        # y_pred = pack_padded_sequence(y_pred, y_len, batch_first=True)
        # y_pred = pad_packed_sequence(y_pred, batch_first=True)[0]
        # use cross entropy loss
        loss = binary_cross_entropy_weight(y_pred, y)
        loss.backward()
        # update deterministic and lstm
        optimizer_output.step()
        optimizer_rnn.step()
        optimizer_basic_dsgc_model.step()
        if args.pygcn:
            optimizer_pygcn_model.step()
        scheduler_output.step()
        scheduler_rnn.step()
        scheduler_basic_dsgc_model.step()
        # temp_print_grad_values(output, 'output')
        # temp_print_grad_values(rnn, 'rnn')
        # temp_print_grad_values(basic_dsgc_model, 'basic_dsgc_model')
        # plot_grad_flow(basic_dsgc_model.named_parameters(), 'basic_dsgc_model')
        # plot_grad_flow(rnn.named_parameters(), 'rnn')
        # plot_grad_flow(output.named_parameters(), 'output')

        if epoch % args.epochs_log == 0 and batch_idx == 0:  # only output first batch's statistics
            print('Epoch: {}/{}, train loss: {:.6f}, graph type: {}, num_layer: {}, hidden: {}'.format(
                epoch, args.epoch, loss.data, args.dataset, args.num_layers, args.hidden_size_rnn))

        # logging
        log_value('DSGC_RNN Training Loss', loss.data, epoch * args.batch_ratio + batch_idx)

        loss_sum += loss.data
    return loss_sum / (batch_idx + 1)


def test_mlp_epoch(args, basic_dsgc_model, pygcn_model, rnn, output, graphs_test, node_features_test,
                   test_batch_size=1):
    graphs_gen = []
    incomplete_graphs = []

    for i in range(len(graphs_test)):
        rnn.hidden = rnn.init_hidden(test_batch_size)

        basic_dsgc_model.eval()
        if args.pygcn:
            pygcn_model.eval()
        rnn.eval()
        output.eval()
        
        graph = graphs_test[i]
        if args.node_features_available:
            feature = node_features_test[i]
        else:
            feature = None
        data = getitem(args, graph, feature, args.max_num_nodes, args.number_of_missing_nodes)
        features = data['features']
        features = torch.tensor(features).cuda().unsqueeze(0).float()
        adj_input = data['adj']
        num_nodes = data['num_nodes']
        # print("***   num_nodes")
        # print(num_nodes)
        # print("***   adj_input before removing")
        # print(adj_input)
        remove_last_n_nodes(args, args.number_of_missing_nodes, num_nodes, adj_input, adj_input)
        # print("***   adj_input after removing")
        # print(adj_input)
        incomplete_graphs.append(get_graph(adj_input, num_nodes=num_nodes - args.number_of_missing_nodes))
        num_nodes = num_nodes - args.number_of_missing_nodes
        if args.pygcn:
            adj_input = torch.tensor(adj_input).cuda().unsqueeze(0).float()
            adj_input, features = pygcn_prepare_data(adj_input.cpu().numpy(), features.cpu().numpy())
            adj_input = adj_input.squeeze(0).astype(float)
            features = torch.from_numpy(features)
        features = Variable(features).cuda()
        adj_input = Variable(torch.from_numpy(np.array(adj_input.copy()))).cuda()
        gnn_features, concatenated_transformer_features = basic_dsgc_model.graph_embedder(features.float(),
                                                                                          adj_input.float(),
                                                                                          pygcn_model=pygcn_model)
        gnn_features = gnn_features[:, :num_nodes, :]
        concatenated_transformer_features = concatenated_transformer_features[:, :num_nodes, :]
        # print("***   gnn_features")
        # print(gnn_features)
        # print("***   concatenated_transformer_features")
        # print(concatenated_transformer_features)
        # generate graphs
        y_pred = Variable(torch.zeros(test_batch_size, num_nodes, args.number_of_missing_nodes)).cuda()
        y_pred_long = Variable(torch.zeros(test_batch_size, num_nodes, args.number_of_missing_nodes)).cuda()
        x_step = Variable(torch.ones(test_batch_size, 1, args.number_of_missing_nodes + args.node_feature_dim)).cuda()
        x_step[:, :, args.number_of_missing_nodes:] = concatenated_transformer_features.cpu()[:, 0, :]
        for i in range(num_nodes):
            # print("####################################   i")
            # print(i)
            # print("***   x_step")
            # print(x_step)
            h = rnn(x_step)
            y_pred_step = output(h)
            y_pred[:, i:i + 1, :] = F.sigmoid(y_pred_step)
            x_step = sample_sigmoid(y_pred_step, sample=True)
            y_pred_long[:, i:i + 1, :] = x_step
            # print("***   y_pred_long")
            # print(y_pred_long)
            if i + 1 < num_nodes:
                x_step = torch.cat((x_step, concatenated_transformer_features[:, i + 1, :].unsqueeze(1)), 2)
            rnn.hidden = Variable(rnn.hidden.data).cuda()
        y_pred_long_data = y_pred_long.data.long()
        all_missing_nodes_labels_prediction = y_pred_long_data.squeeze(0).cpu().detach().numpy()
        # print("&&&   all_missing_nodes_labels_prediction")
        # print(all_missing_nodes_labels_prediction)
        adj_input = adj_input.cpu().detach().numpy()
        adj_input[:num_nodes, num_nodes:num_nodes + args.number_of_missing_nodes] = all_missing_nodes_labels_prediction
        adj_input[num_nodes:num_nodes + args.number_of_missing_nodes,
        :num_nodes] = all_missing_nodes_labels_prediction.transpose()
        # print("***   adj_input after recovering missing nodes")
        # print(adj_input)
        if args.predict_links_between_missing_nodes:
            adj_without_links_between_missing_nodes = torch.tensor(adj_input).cuda()
            # print("***   adj_without_links_between_missing_nodes")
            # print(adj_without_links_between_missing_nodes)
            if args.pygcn:
                adj_without_links_between_missing_nodes, features = pygcn_prepare_data(
                    adj_without_links_between_missing_nodes.unsqueeze(0).cpu().numpy(), features.cpu().numpy())
                adj_without_links_between_missing_nodes = torch.from_numpy(
                    adj_without_links_between_missing_nodes).cuda().squeeze(0)
                features = torch.from_numpy(features).cuda()
            gnn_features_with_missing, concatenated_transformer_features_with_missing = basic_dsgc_model.graph_embedder(
                features.float(), adj_without_links_between_missing_nodes.float(), pygcn_model=pygcn_model)
            # print("***   gnn_features_with_missing")
            # print(gnn_features_with_missing)
            # print("***   concatenated_transformer_features_with_missing")
            # print(concatenated_transformer_features_with_missing)
            y_pred_long = Variable(
                torch.zeros(test_batch_size, args.number_of_missing_nodes, args.number_of_missing_nodes)).cuda()
            for i in range(args.number_of_missing_nodes):
                x_step = torch.cat(
                    (x_step, concatenated_transformer_features_with_missing[:, num_nodes + i, :].unsqueeze(1)), 2)
                # print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^   i")
                # print(i)
                # print("***   x_step")
                # print(x_step)
                h = rnn(x_step)
                y_pred_step = output(h)
                x_step = sample_sigmoid(y_pred_step, sample=True)
                y_pred_long[:, i:i + 1, :] = x_step
                # print("***   y_pred_long")
                # print(y_pred_long)
                rnn.hidden = Variable(rnn.hidden.data).cuda()
            y_pred_long_data = y_pred_long.data.long()
            links_between_missing_nodes = y_pred_long_data.squeeze(0).cpu().detach().numpy()
            # print("***   links_between_missing_nodes")
            # print(links_between_missing_nodes)
            # fill the diagonal of the matrix with all ones
            # np.fill_diagonal(links_between_missing_nodes, 1)

            adj_input = adj_without_links_between_missing_nodes.cpu().detach().numpy()
            adj_input[num_nodes:num_nodes + args.number_of_missing_nodes,
            num_nodes:num_nodes + args.number_of_missing_nodes] = links_between_missing_nodes
            # print("***   fully recovered adj_input")
            # print(adj_input)
        # graphs_gen.append(get_graph(adj_input, num_nodes=num_nodes + args.number_of_missing_nodes))
        graphs_gen.append(get_graph(adj_input, num_nodes=num_nodes + args.number_of_missing_nodes))

    return graphs_gen, incomplete_graphs


def train(args, dataset_train, graphs_test, node_features_test, basic_dsgc_model, pygcn_model, rnn, output):
    epoch = 1

    # initialize optimizer
    optimizer_pygcn_model = None
    if args.pygcn:
        optimizer_pygcn_model = optim.Adam(pygcn_model.parameters(), lr=args.pygcn_lr,
                                           weight_decay=args.pygcn_weight_decay)
    optimizer_basic_dsgc_model = optim.Adam(list(basic_dsgc_model.parameters()), lr=args.lr)
    optimizer_rnn = optim.Adam(list(rnn.parameters()), lr=args.lr)
    optimizer_output = optim.Adam(list(output.parameters()), lr=args.lr)

    scheduler_basic_dsgc_model = MultiStepLR(optimizer_basic_dsgc_model, milestones=args.milestones, gamma=args.lr_rate)
    scheduler_rnn = MultiStepLR(optimizer_rnn, milestones=args.milestones, gamma=args.lr_rate)
    scheduler_output = MultiStepLR(optimizer_output, milestones=args.milestones, gamma=args.lr_rate)

    # train
    while epoch <= args.epoch:
        train_mlp_epoch(epoch, args, basic_dsgc_model, pygcn_model, rnn, output, dataset_train,
                        optimizer_basic_dsgc_model, optimizer_pygcn_model, optimizer_rnn, optimizer_output,
                        scheduler_basic_dsgc_model, scheduler_rnn, scheduler_output)
        epoch += 1
    # test after training
    graphs_test_list = []
    incomplete_graphs_list = []
    graphs_gen_list = []
    for j in range(args.number_of_test_iterations):
        print("Test Iteration: " + str(j))
        for sample_time in range(1):  # currently we proceed with the default value of sample_time parameter of the
            # sample_sigmoid() function which is 2
            graphs_gen, incomplete_graphs = test_mlp_epoch(args, basic_dsgc_model, pygcn_model, rnn, output,
                                                           graphs_test, node_features_test)
            graphs_test_list.extend(graphs_test)
            incomplete_graphs_list.extend(incomplete_graphs)
            graphs_gen_list.extend(graphs_gen)
    print("***   Is data valid?")
    print(check_data_validity(args.number_of_missing_nodes, graphs_test_list, incomplete_graphs_list, graphs_gen_list))
    save_generated_graphs(args, graphs_gen_list)
    save_original_and_incomplete_graphs(args, graphs_test_list, incomplete_graphs_list)
    ged_library = 'ged4py'  # or 'GMatch4py'
    mean_ged_score, average_of_stds = get_average_ged_std_for_test_iterations(graphs_test_list, graphs_gen_list,
                                                                              args.number_of_test_iterations,
                                                                              ged_library)
    print("****   mean_ged_score:")
    print(mean_ged_score)
    print("****   average_of_stds:")
    print(average_of_stds)

    print("----------------------   Rounded Results:")
    print(round(mean_ged_score, 4))
    print(round(average_of_stds, 4))

    # mmd_degree, mmd_clustering, mmd_4orbits, mmd_spectral = mmd_evaluate(graphs_test_list, graphs_gen_list,
    #                                                                      degree_only=False)
    # print("*** mmd_clustering - mmd_degree - mmd_spectral : " + str(mmd_clustering) + " _ " + str(
    #     mmd_degree) + " _ " + str(mmd_spectral))
    print("-----------   number of missing nodes = " + str(args.number_of_missing_nodes))
    print(args.dataset)
