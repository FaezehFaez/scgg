import sys
from pathlib import Path

import torch

from utils.evaluation import mmd_evaluate, compute_ged

sys.path.append(str(Path(__file__).resolve().parent.parent))

from statistics import mean

from tensorboard_logger import log_value
from torch import optim
from torch.autograd import Variable
from torch.optim.lr_scheduler import MultiStepLR

from dsgc.dsgc_evaluate import evaluate as evaluator2
from dsgc.dsgc_util import *


def train(args, dataloader, graphs_test, node_features_test, model, LR_milestones):
    optimizer = optim.Adam(list(model.parameters()), lr=args.lr)
    scheduler = MultiStepLR(optimizer, milestones=LR_milestones, gamma=0.1)
    # scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=100*args.batch_ratio, gamma=0.1, last_epoch=-1)
    for epoch in range(args.epoch):
        model.train()
        for batch_idx, data in enumerate(dataloader):
            model.zero_grad()
            features = data['features'].float()
            batch_num_nodes = data['num_nodes'].int().numpy()
            adj_input = data['adj'].float()

            features = Variable(features).cuda()
            adj_input = Variable(adj_input).cuda()

            loss = model(features, adj_input, batch_num_nodes)
            print('Epoch: ', epoch, ', Iter: ', batch_idx, ', Loss: ', loss)
            loss.backward()

            optimizer.step()
            scheduler.step()

            # plot_grad_flow(model.transformer_encoder.named_parameters(), "transformer_encoder")

        log_value("Learning Rate", scheduler.get_lr()[0], epoch)
        log_value("Training Loss", loss.data, epoch)
        if epoch % 50 == 0 and epoch != 0:
            fname = args.model_save_path + "DSGC" + str(epoch) + '.dat'
            torch.save(model.state_dict(), fname)
    # testing
    test_mae_list = []
    test_precision_list = []
    test_recall_list = []
    test_f1_list = []
    test_roc_score_list = []
    test_ap_score_list = []

    mmd_clustering_test_list = []
    mmd_degree_test_list = []
    mmd_spectral_test_lits = []

    all_true_labels = []
    all_probabilistic_preds = []

    graphs_test_list = []
    incomplete_graphs_list = []
    graphs_gen_list = []

    for j in range(2):
        print("Test Iteration: " + str(j))

        mae, precision, recall, f1, roc_score, ap_score, true_labels, probabilistic_preds, graphs_gen, incomplete_graphs = evaluator2(
            graphs_test, node_features_test, model)
        graphs_test_list.extend(graphs_test)
        incomplete_graphs_list.extend(incomplete_graphs)
        graphs_gen_list.extend(graphs_gen)

        if args.compute_mmd_each_test_iteration:
            mmd_degree_test, mmd_clustering_test, mmd_4orbits_test, mmd_spectral_test = mmd_evaluate(graphs_test,
                                                                                                     graphs_gen,
                                                                                                     degree_only=False)
            mmd_clustering_test_list.append(mmd_clustering_test)
            mmd_degree_test_list.append(mmd_degree_test)
            mmd_spectral_test_lits.append(mmd_spectral_test)

        test_mae_list.append(mae)
        test_precision_list.append(precision)
        test_recall_list.append(recall)
        test_f1_list.append(f1)
        test_roc_score_list.append(roc_score)
        test_ap_score_list.append(ap_score)

        all_true_labels = all_true_labels + true_labels
        all_probabilistic_preds = all_probabilistic_preds + probabilistic_preds

    if model.args.transformer:
        save_original_and_incomplete_graphs(args, graphs_test_list, incomplete_graphs_list)
        ged_library = 'ged4py'  # or 'GMatch4py'
        mean_ged_score, _ = compute_ged(graphs_test_list, graphs_gen_list, '', ged_library)
        print("****   mean_ged_score:")
        print(mean_ged_score)
    method_name_str = "dsgc"
    precision_recall_curve(all_true_labels, all_probabilistic_preds, False, args.dataset, method_name_str)
    if not args.compute_mmd_each_test_iteration:
        mmd_degree_test, mmd_clustering_test, mmd_4orbits_test, mmd_spectral_test = mmd_evaluate(graphs_test,
                                                                                                 graphs_gen,
                                                                                                 degree_only=False)
        mmd_clustering_test_list.append(mmd_clustering_test)
        mmd_degree_test_list.append(mmd_degree_test)
        mmd_spectral_test_lits.append(mmd_spectral_test)

    print("*** MAE - roc_score - ap_score - precision - recall - F_Measure : " + str(
        mean(test_mae_list)) + " _ " + str(mean(test_roc_score_list)) + " _ " + str(
        mean(test_ap_score_list)) + " _ " + str(mean(test_precision_list)) + " _ " + str(
        mean(test_recall_list)) + " _ " + str(mean(test_f1_list)))
    print("*** mmd_clustering_test - mmd_degree_test - mmd_spectral_test : " + str(
        mean(mmd_clustering_test_list)) + " _ " + str(mean(mmd_degree_test_list)) + " _ " + str(
        mean(mmd_spectral_test_lits)))
