import random
import sys
from pathlib import Path

from dsgc import dsgc_create_graphs
from dsgc.dsgc_train import train
from pygcn.models import GCN
from utils.model import MLP_plain, GRU_plain

sys.path.append(str(Path(__file__).resolve().parent.parent))

from time import strftime, gmtime

from random import shuffle
from tensorboard_logger import configure

from dsgc.dsgc_args import DSGC_Args
from dsgc.dsgc_data import GraphAdjSampler
from dsgc.dsgc_model import DSGC
from dsgc.dsgc_util import *
from dsgc.dsgc_rnn_train import train as rnn_train


def build_model(args, max_num_nodes):
    if args.feature_type == 'id':
        input_dim = max_num_nodes
    elif args.feature_type == 'deg':
        input_dim = 1
    elif args.feature_type == 'struct':
        input_dim = 2
    elif args.feature_type == 'feature':
        input_dim = args.node_features_dim
    model = DSGC(input_dim, 16, max_num_nodes, args)
    return model


if __name__ == '__main__':
    LR_milestones = [100, 200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000]

    args = DSGC_Args()
    os.environ['CUDA_VISIBLE_DEVICES'] = str(args.cuda)

    os.environ['PYTHONHASHSEED'] = str(args.seed)
    # Torch RNG
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed(args.seed)
    torch.cuda.manual_seed_all(args.seed)
    # Python RNG
    np.random.seed(args.seed)
    random.seed(args.seed)

    # check if necessary directories exist
    if not os.path.isdir(args.model_save_path):
        os.makedirs(args.model_save_path)
    time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    if args.clean_tensorboard:
        if os.path.isdir("tensorboard"):
            shutil.rmtree("tensorboard")
    configure("tensorboard/run" + time, flush_secs=5)

    graphs, node_features, dataset_name, max_num_nodes = dsgc_create_graphs.create(args)
    args.dataset = dataset_name

    if args.max_num_nodes == -1:
        max_num_nodes = max([graphs[i].number_of_nodes() for i in range(len(graphs))])
    else:
        max_num_nodes = args.max_num_nodes
        # remove graphs with number of nodes greater than max_num_nodes
        graphs = [g for g in graphs if g.number_of_nodes() <= max_num_nodes]
    args.max_num_nodes = max_num_nodes

    # prepare train and test data
    print("-----------   number of missing nodes = " + str(args.number_of_missing_nodes))
    graph_statistics(graphs)
    graphs_len = len(graphs)
    if args.node_features_available:
        intermediate_list = list(zip(graphs, node_features))
        random.shuffle(intermediate_list)
        graphs, node_features = zip(*intermediate_list)
        node_features_train = node_features[0:int(0.8 * graphs_len)]
        node_features_test = node_features[int(0.8 * graphs_len):]
    else:
        shuffle(graphs)
        node_features_train = []
        node_features_test = []
    graphs_test = graphs[int(0.8 * graphs_len):]
    # #################################################################
    kronEM_graphs = []
    for i in range(len(graphs_test)):
        if graphs_test[i].number_of_nodes() == 8 or graphs_test[i].number_of_nodes() == 16 or \
                graphs_test[i].number_of_nodes() == 32 or graphs_test[i].number_of_nodes() == 64 or graphs_test[
            i].number_of_nodes() == 128:
            kronEM_graphs.append(graphs_test[i])
    # prepare_kronEM_data(kronEM_graphs, prog_args.dataset, True)
    # #################################################################
    graphs_train = graphs[0:int(0.8 * graphs_len)]
    save_graphs_as_mat(graphs_test)
    print('total graph num: {}, training set: {}'.format(len(graphs), len(graphs_train)))
    dataset = GraphAdjSampler(graphs_train, node_features_train, max_num_nodes, args)
    sample_strategy = torch.utils.data.sampler.WeightedRandomSampler([1.0 / len(dataset) for i in range(len(dataset))],
                                                                     num_samples=args.batch_size * args.batch_ratio,
                                                                     replacement=True)
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, num_workers=args.num_workers,
                                                 sampler=sample_strategy)
    basic_dsgc_model = build_model(args, max_num_nodes).cuda()
    pygcn_model = None
    if args.pygcn:
        if args.node_features_available and args.use_node_features:
            nfeat = args.node_features_dim
        else:
            nfeat = args.max_num_nodes
        pygcn_model = GCN(nfeat=nfeat, nhid1=args.pygcn_hidden1, nhid2=args.pygcn_hidden2,
                          dropout=args.pygcn_dropout).cuda()
    if args.dsgc_rnn_mode:
        rnn = GRU_plain(input_size=args.number_of_missing_nodes + args.node_feature_dim,
                        embedding_size=args.embedding_size_rnn, hidden_size=args.hidden_size_rnn,
                        num_layers=args.num_layers, has_input=True, has_output=False).cuda()
        output = MLP_plain(h_size=args.hidden_size_rnn, embedding_size=args.embedding_size_output,
                           y_size=args.number_of_missing_nodes).cuda()
        rnn_train(args, dataset_loader, graphs_test, node_features_test, basic_dsgc_model, pygcn_model, rnn, output)
    else:
        train(args, dataset_loader, graphs_test, node_features_test, basic_dsgc_model, LR_milestones)
