import math
import statistics

import numpy as np
import sklearn
from sklearn.metrics import mean_absolute_error, precision_recall_fscore_support, average_precision_score

from ged4py.algorithm import graph_edit_dist
from utils.eval_helper import degree_stats, clustering_stats, spectral_stats


def compute_evaluation_metrics(labels, results, probabilistic_preds):
    mae = mean_absolute_error(labels, probabilistic_preds)
    precision, recall, f1, _ = precision_recall_fscore_support(labels, results, average='binary')
    fpr, tpr, threshold = sklearn.metrics.roc_curve(labels, probabilistic_preds)
    roc_score = sklearn.metrics.auc(fpr, tpr)
    ap_score = average_precision_score(labels, probabilistic_preds)
    return mae, precision, recall, f1, roc_score, ap_score


def mmd_evaluate(graph_gt, graph_pred, degree_only=True):
    mmd_degree = degree_stats(graph_gt, graph_pred)

    if degree_only:
        mmd_4orbits = 0.0
        mmd_clustering = 0.0
        mmd_spectral = 0.0
    else:
        # mmd_4orbits = orbit_stats_all(graph_gt, graph_pred)
        mmd_4orbits = 5
        mmd_clustering = clustering_stats(graph_gt, graph_pred)
        mmd_spectral = spectral_stats(graph_gt, graph_pred)

    return mmd_degree, mmd_clustering, mmd_4orbits, mmd_spectral


def compute_ged(original_graph_list, recovered_graph_list, ged_algorithm, ged_library):
    '''
    exactly the same function as what is in  GraphRNN-DeepNC repository
    '''
    ged_list = []
    for i in range(len(original_graph_list)):
        g1 = original_graph_list[i]
        g2 = recovered_graph_list[i]
        if ged_library == 'GMatch4py':
            ged_score_matrix = ged_algorithm.compare([g1, g2], None)
            ged = min(ged_score_matrix[1, 0], ged_score_matrix[0, 1])
            # DeepNC Paper: In our experiments, GED is normalized by the average size of the two graphs.
            mean_number_of_nodes = np.mean([g1.number_of_nodes(), g2.number_of_nodes()])
            ged = ged / mean_number_of_nodes
        elif ged_library == 'ged4py':
            ged = graph_edit_dist.compare(g1, g2, False)
        ged_list.append(ged)
    return statistics.mean(ged_list), statistics.stdev(ged_list)


def get_average_ged_std_for_test_iterations(graphs_test_list, graphs_gen_list, number_of_test_iterations, ged_library):
    mean_ged_list = []
    std_list = []
    number_of_test_graphs = int(len(graphs_test_list) / number_of_test_iterations)
    for i in range(number_of_test_graphs):
        print("Computing GED for the " + str(i + 1) + "-th graph out of " + str(number_of_test_graphs) + " test graphs")
        index_list = [index for index, value in enumerate(graphs_test_list) if index % number_of_test_graphs == i]
        current_graphs_test_list = [value for index, value in enumerate(graphs_test_list) if index in index_list]
        current_graphs_gen_list = [value for index, value in enumerate(graphs_gen_list) if index in index_list]
        mean_ged, std = compute_ged(current_graphs_test_list, current_graphs_gen_list, '', ged_library)
        mean_ged_list.append(mean_ged)
        std_list.append(std)

    # averaging over standard deviations with equal sample sizes
    power_two_std_list = [x ** 2 for x in std_list]
    average_of_stds = math.sqrt(sum(power_two_std_list) / len(power_two_std_list))

    return statistics.mean(mean_ged_list), average_of_stds
