from __future__ import unicode_literals, print_function, division

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init
from torch.autograd import Variable
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence


def binary_cross_entropy_weight(y_pred, y, has_weight=False, weight_length=1, weight_max=10):
    '''

    :param y_pred:
    :param y:
    :param weight_length: how long until the end of sequence shall we add weight
    :param weight_value: the magnitude that the weight is enhanced
    :return:
    '''
    if has_weight:
        weight = torch.ones(y.size(0), y.size(1), y.size(2))
        weight_linear = torch.arange(1, weight_length + 1) / weight_length * weight_max
        weight_linear = weight_linear.view(1, weight_length, 1).repeat(y.size(0), 1, y.size(2))
        weight[:, -1 * weight_length:, :] = weight_linear
        loss = F.binary_cross_entropy(y_pred, y, weight=weight.cuda())
    else:
        loss = F.binary_cross_entropy(y_pred, y)
    return loss


def sample_sigmoid(y, sample, thresh=0.5, sample_time=2):
    '''
        do sampling over unnormalized score
    :param y: input
    :param sample: Bool
    :param thresh: if not sample, the threshold
    :param sampe_time: how many times do we sample, if =1, do single sample
    :return: sampled result
    '''

    # do sigmoid first
    y = torch.sigmoid(y)
    # do sampling
    if sample:
        if sample_time > 1:
            y_result = Variable(torch.rand(y.size(0), y.size(1), y.size(2))).cuda()
            # loop over all batches
            for i in range(y_result.size(0)):
                # do 'multi_sample' times sampling
                for j in range(sample_time):
                    y_thresh = Variable(torch.rand(y.size(1), y.size(2))).cuda()
                    y_result[i] = torch.gt(y[i], y_thresh).float()
                    if (torch.sum(y_result[i]).data > 0).any():
                        break
                    # else:
                    #     print('all zero',j)
        else:
            y_thresh = Variable(torch.rand(y.size(0), y.size(1), y.size(2))).cuda()
            y_result = torch.gt(y, y_thresh).float()
    # do max likelihood based on some threshold
    else:
        y_thresh = Variable(torch.ones(y.size(0), y.size(1), y.size(2)) * thresh).cuda()
        y_result = torch.gt(y, y_thresh).float()
    return y_result


# plain GRU model
class GRU_plain(nn.Module):
    def __init__(self, input_size, embedding_size, hidden_size, num_layers, has_input=True, has_output=False,
                 output_size=None):
        super(GRU_plain, self).__init__()
        self.num_layers = num_layers
        self.hidden_size = hidden_size
        self.has_input = has_input
        self.has_output = has_output

        if has_input:
            self.input = nn.Linear(input_size, embedding_size)
            self.rnn = nn.GRU(input_size=embedding_size, hidden_size=hidden_size, num_layers=num_layers,
                              batch_first=True)
        else:
            self.rnn = nn.GRU(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True)
        if has_output:
            self.output = nn.Sequential(
                nn.Linear(hidden_size, embedding_size),
                nn.ReLU(),
                nn.Linear(embedding_size, output_size)
            )

        self.relu = nn.ReLU()
        # initialize
        self.hidden = None  # need initialize before forward run

        for name, param in self.rnn.named_parameters():
            if 'bias' in name:
                nn.init.constant(param, 0.25)
            elif 'weight' in name:
                nn.init.xavier_uniform(param, gain=nn.init.calculate_gain('sigmoid'))
        for m in self.modules():
            if isinstance(m, nn.Linear):
                m.weight.data = init.xavier_uniform(m.weight.data, gain=nn.init.calculate_gain('relu'))

    def init_hidden(self, batch_size):
        return Variable(torch.zeros(self.num_layers, batch_size, self.hidden_size)).cuda()

    def forward(self, input_raw, pack=False, input_len=None):
        if self.has_input:
            input = self.input(input_raw)
            input = self.relu(input)
        else:
            input = input_raw
        if pack:
            input = pack_padded_sequence(input, input_len, batch_first=True)
        output_raw, self.hidden = self.rnn(input, self.hidden)
        if pack:
            output_raw = pad_packed_sequence(output_raw, batch_first=True)[0]
        if self.has_output:
            output_raw = self.output(output_raw)
        # return hidden state at each time step
        return output_raw


# a deterministic linear output
class MLP_plain(nn.Module):
    def __init__(self, h_size, embedding_size, y_size):
        super(MLP_plain, self).__init__()
        self.deterministic_output = nn.Sequential(
            nn.Linear(h_size, embedding_size),
            nn.ReLU(),
            nn.Linear(embedding_size, y_size)
        )

        for m in self.modules():
            if isinstance(m, nn.Linear):
                m.weight.data = init.xavier_uniform(m.weight.data, gain=nn.init.calculate_gain('relu'))

    def forward(self, h):
        y = self.deterministic_output(h)
        return y


class Main_MLP_VAE_plain(nn.Module):
    def __init__(self, h_size, embedding_size, y_size):
        super(Main_MLP_VAE_plain, self).__init__()
        self.encode_11 = nn.Linear(h_size, embedding_size)  # mu
        self.encode_12 = nn.Linear(h_size, embedding_size)  # lsgms

        self.decode_1 = nn.Linear(embedding_size, embedding_size)
        self.decode_2 = nn.Linear(embedding_size, y_size)  # make edge prediction (reconstruct)
        self.relu = nn.ReLU()

        for m in self.modules():
            if isinstance(m, nn.Linear):
                m.weight.data = init.xavier_uniform(m.weight.data, gain=nn.init.calculate_gain('relu'))

    def forward(self, h):
        # encoder
        z_mu = self.encode_11(h)
        z_lsgms = self.encode_12(h)
        # reparameterize
        z_sgm = z_lsgms.mul(0.5).exp_()
        eps = Variable(torch.randn(z_sgm.size())).cuda()
        z = eps * z_sgm + z_mu
        # decoder
        # print("###   z ")
        # print(z)
        y = self.decode_1(z)
        # print("###   decode_1 ")
        # print(y)
        y = self.relu(y)
        y = self.decode_2(y)
        # print("###   decode_2 ")
        # print(y)
        return y, z_mu, z_lsgms


# a deterministic linear output (update: add noise)
class MLP_VAE_plain(nn.Module):
    def __init__(self, h_size, embedding_size, y_size):
        super(MLP_VAE_plain, self).__init__()
        self.encode_11 = nn.Linear(h_size, embedding_size)  # mu
        self.encode_12 = nn.Linear(h_size, embedding_size)  # lsgms
        self.decode_1 = nn.Linear(embedding_size, 32)
        self.linear = nn.Linear(h_size, y_size)
        self.bn1 = nn.BatchNorm1d(32)
        # self.decode_1_2 = nn.Linear(256, 512)
        # self.bn2 = nn.BatchNorm1d(512)
        self.decode_2 = nn.Linear(32, y_size)  # make edge prediction (reconstruct)
        self.relu = nn.ReLU()

        for m in self.modules():
            if isinstance(m, nn.Linear):
                m.weight.data = init.xavier_uniform(m.weight.data, gain=nn.init.calculate_gain('relu'))

    def forward(self, h):
        # encoder
        z_mu = self.encode_11(h)
        z_lsgms = self.encode_12(h)
        # reparameterize
        z_sgm = z_lsgms.mul(0.5).exp_()
        eps = Variable(torch.randn(z_sgm.size())).cuda()
        # z = eps*z_sgm + z_mu
        z = z_mu
        # decoder
        y = self.decode_1(z)
        # y = self.bn(y)
        y = self.relu(y)
        # y = self.decode_1_2(y)
        # y = self.bn2(y)
        y = self.relu(y)
        y = self.decode_2(y)
        # reconstruction = True
        # if reconstruction:
        #     y = self.linear(h)
        return y, z_mu, z_lsgms


class GRAN(nn.Module):
    def __init__(self, max_num_nodes, transformer, transformer_after_gcn, hidden_dim, dropout=0):
        super(GRAN, self).__init__()
        self.max_num_nodes = max_num_nodes
        self.hidden_dim = hidden_dim
        self.num_mix_component = 1
        self.output_dim = 1
        if transformer_after_gcn or not transformer:
            input_dim = 2 * self.hidden_dim
        else:
            input_dim = max_num_nodes + self.hidden_dim
        self.output_theta = nn.Sequential(
            nn.Linear(input_dim, self.hidden_dim),
            nn.ReLU(inplace=True),
            nn.Linear(self.hidden_dim, self.hidden_dim),
            nn.ReLU(inplace=True),
            nn.Linear(self.hidden_dim, self.output_dim * self.num_mix_component), nn.Dropout(p=dropout))

    def forward(self, h, batch_num_nodes, random_index_list, GRAN_arbitrary_node):
        batch_size = h.size()[0]
        diff = Variable(torch.zeros(batch_size, self.max_num_nodes - 1, h.size()[2]).cuda())
        concat = Variable(torch.zeros(batch_size, self.max_num_nodes - 1, 2 * h.size()[2]).cuda())
        for i in range(batch_size):
            diff[i, :batch_num_nodes[i] - 1, :] = h[i, :batch_num_nodes[i] - 1, :] - h[i, batch_num_nodes[i] - 1, :]
            diff[i, batch_num_nodes[i] - 1:, :] = -100
            h_duplicate = h[i, batch_num_nodes[i] - 1, :].unsqueeze(0).repeat(1, batch_num_nodes[i] - 1, 1).squeeze()
            concat[i, :batch_num_nodes[i] - 1, :] = torch.cat(
                (h[i, :batch_num_nodes[i] - 1, :], h_duplicate), 1)
            if GRAN_arbitrary_node:
                h_duplicate = h[i, random_index_list[i], :].unsqueeze(0).repeat(1, batch_num_nodes[i] - 1,
                                                                                1).squeeze()
                concat[i, :random_index_list[i], :] = torch.cat(
                    (h[i, :random_index_list[i], :], h_duplicate[:random_index_list[i], :]), 1)
                concat[i, random_index_list[i]:batch_num_nodes[i] - 1, :] = torch.cat(
                    (h[i, random_index_list[i] + 1:batch_num_nodes[i], :],
                     h_duplicate[random_index_list[i]:batch_num_nodes[i] - 1, :]), 1)
        # return self.output_theta(diff)
        # print("***  h")
        # print(h)
        # print("***  concat")
        # print(concat)
        return self.output_theta(concat)


# GCN basic operation
class BasicGraphConv(nn.Module):
    def __init__(self, input_dim, output_dim):
        super(BasicGraphConv, self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.weight = nn.Parameter(torch.FloatTensor(input_dim, output_dim).cuda())
        # self.relu = nn.ReLU()

    def forward(self, x, adj):
        y = torch.matmul(adj, x)
        y = torch.matmul(y, self.weight)
        return y


class GraphConv(nn.Module):
    def __init__(self, input_dim, output_dim, add_self=False, normalize_embedding=False,
                 dropout=0.0, bias=True):
        super(GraphConv, self).__init__()
        self.add_self = add_self
        self.dropout = dropout
        if dropout > 0.001:
            self.dropout_layer = nn.Dropout(p=dropout)
        self.normalize_embedding = normalize_embedding
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.weight = nn.Parameter(torch.FloatTensor(input_dim, output_dim).cuda())
        if bias:
            self.bias = nn.Parameter(torch.FloatTensor(output_dim).cuda())
        else:
            self.bias = None

    def forward(self, x, adj):
        if self.dropout > 0.001:
            x = self.dropout_layer(x)
        y = torch.matmul(adj, x)
        if self.add_self:
            y += x
        y = torch.matmul(y, self.weight)
        if self.bias is not None:
            y = y + self.bias
        if self.normalize_embedding:
            y = F.normalize(y, p=2, dim=2)
        return y
